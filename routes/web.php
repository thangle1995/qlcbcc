<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/confirm/{eventStaffId}/{token}', 'EventController@eventConfirm');
Route::get('/login', 'UserAdminController@Login');
Route::get('/logout', 'UserAdminController@Logout');
Route::post('/login', 'UserAdminController@PostLogin');
Route::group(['middleware'=>'admin'],function(){
//    Route::get('/', 'DepartmentController@ListDepartment');
    Route::get('/', 'DepartmentController@Index');
    Route::get('staff/leave-of-absence', 'UserAdminController@getLeaveOfAbsence');
    Route::post('staff/leave-of-absence', 'UserAdminController@postLeaveOfAbsence');
    Route::prefix('don-nghi')->group(function () {
        Route::get('/danh-sach', 'UserAdminController@ListLeaveOfAbsence');
        Route::get('/tao-moi', 'UserAdminController@getLeaveOfAbsence');
        Route::post('tao-moi', 'UserAdminController@postLeaveOfAbsence');
        Route::get('/cap-nhat/{LeaveOfAbsenceId}', 'UserAdminController@EditLeaveOfAbsence');
        Route::post('/cap-nhat/{LeaveOfAbsenceId}', 'UserAdminController@UpdateLeaveOfAbsence');
        Route::get('/xoa/{LeaveOfAbsenceId}', 'UserAdminController@DeleteLeaveOfAbsence');
    });
    //chấm công của user
    Route::prefix('cham-cong')->group(function () {
        Route::get('/staff', 'UserAdminController@KeepingStaff');
        Route::post('/staff', 'UserAdminController@PostKeepingStaff');
        Route::post('/staff/ly-do', 'UserAdminController@PostKeepingReason');
    });
    //Quản trị
    Route::prefix('quan-tri')->group(function () {
        Route::get('/danh-sach', 'UserAdminController@ListUserAdmin');
        Route::get('/tao-moi', 'UserAdminController@Create');
        Route::get('/cap-nhat/{UserAdminId}', 'UserAdminController@Edit');
        Route::get('/xoa/{UserAdminId}', 'UserAdminController@Delete');
        Route::post('/tao-moi', 'UserAdminController@Storage');
        Route::post('/cap-nhat/{UserAdminId}', 'UserAdminController@Update');
        Route::get('/thong-tin', 'UserAdminController@Profile');
        Route::post('/thong-tin', 'UserAdminController@UpdateProfile');
        Route::get('/change-pass', 'UserAdminController@ChangePass');
        Route::post('/change-pass', 'UserAdminController@PostChangePass');
        Route::get('/profile-home', 'UserAdminController@ProfileHome');

    });
    //Chức vụ
    Route::prefix('quyen')->group(function () {
        Route::get('/danh-sach', 'PermissionController@ListPermission');
        Route::get('/tao-moi', 'PermissionController@Create');
        Route::get('/cap-nhat/{PermissionId}', 'PermissionController@Edit');
        Route::get('/xoa/{PermissionId}', 'PermissionController@Delete');
        Route::post('/tao-moi', 'PermissionController@Storage');
        Route::post('/cap-nhat/{PermissionId}', 'PermissionController@Update');
    });
    //Phòng ban
    Route::prefix('phong-ban')->group(function () {
        Route::get('/danh-sach', 'DepartmentController@ListDepartment');
        Route::get('/don-xin-nghi', 'DepartmentController@ListLeaveOfAbsence');
        Route::get('/don-xin-nghi/{LeaveOfAbsenceId}', 'DepartmentController@EditLeaveOfAbsence');
        Route::post('/don-xin-nghi/{LeaveOfAbsenceId}', 'DepartmentController@UpdateLeaveOfAbsence');
        Route::get('/tao-moi', 'DepartmentController@Create');
        Route::get('/cap-nhat/{DepartmentId}', 'DepartmentController@Edit');
        Route::get('/xoa/{DepartmentId}', 'DepartmentController@Delete');
        Route::post('/them', 'DepartmentController@Storage');
        Route::post('/cap-nhat/{DepartmentId}', 'DepartmentController@Update');
    });
    //Sự kiện
    Route::prefix('su-kien')->group(function () {
        Route::get('/danh-sach', 'EventController@ListEvent');

        Route::get('/tao-moi', 'EventController@Create');
        Route::get('/cap-nhat/{EventId}', 'EventController@Edit');
        Route::get('/xoa/{EventId}', 'EventController@Delete');
        Route::post('/tao-moi', 'EventController@Storage');
        Route::post('/cap-nhat/{EventId}', 'EventController@Update');
    });
    //Thẻ
    Route::prefix('the')->group(function () {
        Route::get('/danh-sach', 'CardController@ListCard');
        Route::get('/tao-moi', 'CardController@Create');
        Route::get('/cap-nhat/{MifareCardId}', 'CardController@Edit');
        Route::get('/xoa/{MifareCardId}', 'CardController@Delete');
        Route::post('/tao-moi', 'CardController@Storage');
        Route::post('/cap-nhat/{MifareCardId}', 'CardController@Update');
    });
    //Loại thẻ
    Route::prefix('loai-the')->group(function () {
        Route::get('/danh-sach', 'CardTypeController@ListCardType');
        Route::get('/tao-moi', 'CardTypeController@Create');
        Route::get('/cap-nhat/{MifareCardTypeId}', 'CardTypeController@Edit');
        Route::get('/xoa/{MifareCardTypeId}', 'CardTypeController@Delete');
        Route::post('/tao-moi', 'CardTypeController@Storage');
        Route::post('/cap-nhat/{MifareCardTypeId}', 'CardTypeController@Update');
    });
    //Vai trò
    Route::prefix('vai-tro')->group(function () {
        Route::get('/danh-sach', 'RoleController@ListRole');
        Route::get('/tao-moi', 'RoleController@Create');
        Route::get('/cap-nhat/{RoleId}', 'RoleController@Edit');
        Route::get('/xoa/{RoleId}', 'RoleController@Delete');
        Route::post('/tao-moi', 'RoleController@Storage');
        Route::post('/cap-nhat/{RoleId}', 'RoleController@Update');
    });
    //Chức vụ
    Route::prefix('chuc-vu')->group(function () {
        Route::get('/danh-sach', 'StaffTypeController@ListStaffType');
        Route::get('/tao-moi', 'StaffTypeController@Create');
        Route::get('/cap-nhat/{StaffTypeId}', 'StaffTypeController@Edit');
        Route::get('/xoa/{StaffTypeId}', 'StaffTypeController@Delete');
        Route::post('/tao-moi', 'StaffTypeController@Storage');
        Route::post('/cap-nhat/{StaffTypeId}', 'StaffTypeController@Update');
    });
    //Nhân viên
    Route::prefix('nhan-vien')->group(function () {
        Route::get('/danh-sach', 'StaffController@ListStaff');
        Route::get('/danh-sach/{DepartmentId}', 'StaffController@ListStaffByDepartment');
        Route::get('/tao-moi', 'StaffController@Create');
        Route::get('/cap-nhat/{StaffId}', 'StaffController@Edit');
        Route::get('/xoa/{StaffId}', 'StaffController@Delete');
        Route::post('/tao-moi', 'StaffController@Storage');
        Route::post('/cap-nhat/{StaffId}', 'StaffController@Update');
        Route::post('/import', 'StaffController@postImportStaffExcel');
        Route::get('/export', 'StaffController@ExportStaff');
    });
    //Nhân viên
    Route::prefix('ngay-nghi')->group(function () {
        Route::get('/trong-tuan', 'ConfigDayOffController@ListDayOffInWeek');
        Route::post('/trong-tuan', 'ConfigDayOffController@PostDayOffInWeek');
        Route::get('/ngay-le', 'ConfigDayOffController@ListHoliday');
        Route::post('/ngay-le/cap-nhat', 'ConfigDayOffController@postHoliday');
        Route::get('/ngay-le/xoa/{HolidayId}', 'ConfigDayOffController@deleteHoliday');
        Route::get('/danh-sach/{DepartmentId}', 'StaffController@ListStaffByDepartment');
        Route::get('/tao-moi', 'StaffController@Create');
        Route::get('/cap-nhat/{StaffId}', 'StaffController@Edit');
        Route::get('/xoa/{StaffId}', 'StaffController@Delete');
        Route::post('/tao-moi', 'StaffController@Storage');
        Route::post('/cap-nhat/{StaffId}', 'StaffController@Update');
        Route::post('/import', 'StaffController@postImportStaffExcel');
        Route::get('/export', 'StaffController@ExportStaff');
    });
    //Chấm công
    Route::prefix('thong-ke')->group(function () {
        Route::get('/cham-cong', 'DepartmentController@ListDepartmentKeeping');
        Route::get('/cham-cong/{DepartmentId}', 'DepartmentController@ListStaffByDepartment');
        Route::post('/cham-cong/{DepartmentId}', 'DepartmentController@ListStaffByDepartmentMonth');
        Route::get('/cham-cong/staff/{StaffId}', 'DepartmentController@KeepingStaff');
        Route::post('/cham-cong/staff/{StaffId}', 'DepartmentController@PostKeepingStaff');
        Route::post('/cham-cong/cap-nhat/ly-do', 'DepartmentController@PostKeepingReason');

        Route::get('/su-kien', 'EventController@ListEventReport');
        Route::get('/su-kien/{EventId}', 'EventController@ListEventStaff');
        Route::get('/su-kien/xoa/{EventId}/{StaffId}', 'EventController@DeleteEventStaff');
        Route::get('/su-kien/{EventId}', 'EventController@ListEventStaff');
        Route::get('/su-kien/xoa/{EventId}/{StaffId}', 'EventController@DeleteEventStaff');
//        Route::get('/cap-nhat/{StaffId}', 'StaffController@Edit');
//        Route::get('/xoa/{StaffId}', 'StaffController@Delete');
//        Route::post('/tao-moi', 'StaffController@Storage');
//        Route::post('/cap-nhat/{StaffId}', 'StaffController@Update');
//        Route::post('/import', 'StaffController@postImportStaffExcel');
//        Route::get('/export', 'StaffController@ExportStaff');
    });
    //Thông báo
    Route::prefix('thong-bao')->group(function () {
        Route::get('/su-kien', 'NotificationController@ListEvent');
        Route::get('/su-kien/{EventId}', 'NotificationController@EventDetail');
        Route::get('/su-kien/accept/{EventId}', 'NotificationController@EventAccept');
        Route::get('/su-kien/propose/{EventId}', 'NotificationController@ProposeStaff');
        Route::post('/su-kien/propose/{EventId}', 'NotificationController@postProposeStaff');

        Route::get('/nhan-vien', 'NotificationController@ListStaff');
        Route::get('/nhan-vien/{StaffId}', 'NotificationController@StaffDetail');
    });

});


