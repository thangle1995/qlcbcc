<?php

namespace App\Jobs;

use App\Mail\SendConfirmEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendConfirmMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $email;
    protected $staff_name;
    protected $event_name;
    protected $start;
    protected $end;
    protected $address;
    protected $description;
    protected $event_staff_id;
    protected $token;
    public $tries = 3;
    public $timeout = 60;

    public function __construct($email, $staff_name, $event_name, $start, $end, $address, $description, $event_staff_id, $token)
    {
        $this->email = $email;
        $this->staff_name = $staff_name;
        $this->event_name = $event_name;
        $this->start = $start;
        $this->end = $end;
        $this->address = $address;
        $this->description = $description;
        $this->event_staff_id = $event_staff_id;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email_content = new SendConfirmEvent($this->staff_name, $this->event_name, $this->start, $this->end, $this->address, $this->description,$this->event_staff_id,$this->token);
        Mail::to($this->email)->send($email_content);
    }
}
