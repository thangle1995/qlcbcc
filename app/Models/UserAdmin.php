<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAdmin extends Model
{
    //
    public $timestamps = false;
    protected $table = 'UserAdmin';
    public function setPasswordAttribute($password)
    {
        $this->attributes['Password'] = bcrypt($password);
    }
}
