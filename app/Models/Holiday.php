<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    //
    public $timestamps = true;
    protected $table = 'Holiday';
}
