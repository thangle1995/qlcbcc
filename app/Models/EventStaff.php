<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventStaff extends Model
{
    //
    public $timestamps = false;
    protected $table = 'EventStaff';
    //status =1 : được mời, đang đợi
    //status=0: Đã đề xuất người khác
    //status=2: chấp nhận
}
