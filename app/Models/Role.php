<?php
/**
 * Created by Thắng Lê.
 * User: BSTAR
 * Date: 5/3/2018
 * Time: 3:47 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Role extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Role';

}