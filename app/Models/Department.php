<?php
/**
 * Created by Thắng Lê.
 * User: BSTAR
 * Date: 5/3/2018
 * Time: 3:47 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Department extends Model
{
    //
    public $timestamps = false;
    protected $table = 'Department';
    public function count_staff(){
        return $this->hasMany('App\Models\Staff','DepartmentId','DepartmentId')->count();
    }
    public function staff(){
        return $this->hasMany('App\Models\Staff','DepartmentId','DepartmentId')->get();
    }
}