<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    public $timestamps = true;
    protected $table = 'Event';
}
