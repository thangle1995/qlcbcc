<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveOfAbsence extends Model
{
    //
    public $timestamps = true;
    protected $table = 'LeaveOfAbsence';
    public function reStaff(){
        return $this->belongsTo('App\Models\Staff','StaffId','StaffId');
    }
}
