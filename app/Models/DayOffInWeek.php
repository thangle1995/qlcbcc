<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DayOffInWeek extends Model
{
    //
    public $timestamps = true;
    protected $table = 'DayOffInWeek';
}
