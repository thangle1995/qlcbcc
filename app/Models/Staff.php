<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Staff extends Model
{
    //
    public $timestamps = true;
    protected $table = 'Staff';
    protected $hidden = [
        'password', 'token',
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['Password'] = bcrypt($password);
    }
    public function reTypeStaff(){
        return $this->belongsTo('App\Models\StaffType','StaffTypeId','StaffTypeId');
    }

    public function reDepartment(){
        return $this->belongsTo('App\Models\Department','DepartmentId','DepartmentId');
    }
    public function reKeeping(){
        return $this->hasMany('App\Models\KeepingTime','StaffId','StaffId');
    }
}