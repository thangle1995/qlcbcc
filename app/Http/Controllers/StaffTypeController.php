<?php

namespace App\Http\Controllers;

use App\Models\StaffType;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;

class StaffTypeController extends Controller
{
    //
    public function ListStaffType()
    {
        $role = StaffType::all()->sortByDesc('StaffTypeId');
        if ($role) {
            return view('stafftype/index', [
                'stafftype' => $role
            ]);
        }
    }
    public function Create()
    {
        return view('stafftype/create');
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('StaffTypeName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên chức vụ')->withInput();
        $role = new StaffType();
        $role->StaffTypeName = StringHelper::convertUtf8($request->get('StaffTypeName'));
        $role->Type = $request->get('Type') ? 1 : 0;
        if ($role->save()) {
            return redirect('/chuc-vu/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($StaffTypeId)
    {
        $role = StaffType::where('StaffTypeId', $StaffTypeId)->first();
        if ($role) {
            return view('stafftype/edit', [
                'stafftype' => $role
            ]);
        }

    }

    public function Update(Request $request, $StaffTypeId)
    {
        if (empty($request->get('StaffTypeName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên chức vụ')->withInput();

        DB::table('StaffType')->where('StaffTypeId', $StaffTypeId)->update([
            'StaffTypeName' => StringHelper::convertUtf8($request->get('StaffTypeName')),
            'Type'=>$request->get('Type') ? 1 : 0

        ]);
//        Session::flash('message','Cập nhật thành công');
        return redirect('/chuc-vu/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($StaffTypeId)
    {
        try{
            $role = StaffType::where('StaffTypeId', $StaffTypeId)->delete();
            return redirect('/chuc-vu/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/chuc-vu/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
