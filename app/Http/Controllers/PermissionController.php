<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    //
    public function ListPermission()
    {
        $permission = Permission::where('ParentId',0)->orderBy('PermissionId')->get();
        foreach ($permission as $key=>$p){
            $child=Permission::where('ParentId',$p->PermissionId)->orderBy('Name')->get();
            $permission[$key]->child=$child;
        }
        if ($permission) {
            return view('permission/index', [
                'permission' => $permission
            ]);
        }
    }
    public function Create()
    {
        $category=Permission::where('ParentId',0)->get();
        return view('permission/create',['category'=>$category]);
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('Name')))
            return redirect()->back()->with('error','Vui lòng không để trống tên quyền')->withInput();
        if (empty($request->get('Link')))
            return redirect()->back()->with('error','Vui lòng không để trống link')->withInput();
        $permission = new Permission();
        $permission->Name = StringHelper::convertUtf8($request->get('Name'));
        $permission->Link = StringHelper::convertUtf8($request->get('Link'));
        $permission->ParentId = $request->get('ParentId');
//        $cardtype->IsActive = $request->get('IsActive');
        if ($permission->save()) {
            return redirect('/quyen/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($PermissionId)
    {
        $permission = Permission::where('PermissionId', $PermissionId)->first();
        if ($permission) {
            $category=Permission::where('ParentId',0)->get();
            return view('permission/edit', [
                'permission' => $permission,
                'category'=>$category
            ]);
        }

    }

    public function Update(Request $request, $permissionId)
    {
        if (empty($request->get('Name')))
            return redirect()->back()->with('error','Vui lòng không để trống tên quyền')->withInput();
        if (empty($request->get('Link')))
            return redirect()->back()->with('error','Vui lòng không để trống link')->withInput();

        DB::table('Permission')->where('PermissionId', $permissionId)->update([
            'Name' => StringHelper::convertUtf8($request->get('Name')),
            'Link' => StringHelper::convertUtf8($request->get('Link')),
            'ParentId' => $request->get('ParentId'),

        ]);
//        Session::flash('message','Cập nhật thành công');
        return redirect('/quyen/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($PermissionId)
    {
        try{
            $permission = Permission::where('PermissionId', $PermissionId)->delete();
            return redirect('/quyen/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/quyen/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
