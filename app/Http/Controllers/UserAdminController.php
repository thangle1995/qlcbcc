<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\KeepingTime;
use App\Models\LeaveOfAbsence;
use App\Models\MifareCard;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Staff;
use App\Models\StaffType;
use App\Models\UserPermission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\UserAdmin;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserAdminController extends Controller
{
    //
    public function ProfileHome(){
        $staff=session('admin');
        return view('useradmin.profile-home');
    }
    public function Login(){
        if (session('admin'))
            return redirect('/');
        return view('login');
    }
    public function Logout(){
        session()->forget('admin');
        return redirect('/login');
    }
    public function getDate(){
        $date=[
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
        ];
        return $date;
    }
    public function getWeek($month, $year){
        $list=array();
        $week=array();
        $i=0;
        for($d=1; $d<=31; $d++)
        {
            $time=mktime(12, 0, 0, $month, $d, $year);
            if (date('m', $time)==$month){
                $list[]=date('Y-m-d-D', $time);
                $week[$i][date('D', $time)]=date('Y-m-d', $time);
                if(date('D', $time)=='Sat'){
                    $i++;
                }
            }
        }
        return $week;
    }
    public function KeepingStaff(){
        $staff=session('admin');
        if($staff){
            $StaffId=$staff->StaffId;
            $date=$this->getDate();
            $now = Carbon::now();
            $month = $now->month;
            $year = $now->year;
            $week=$this->getWeek($month, $year);
            $staff=Staff::where('StaffId',$StaffId)->first();
            $keepingTime= $keepingTime=KeepingTime::where('StaffId',$StaffId)
                ->whereMonth('KeepingTime',$month)
                ->whereYear('KeepingTime',$year)->get();
            return view('staff/staff_keeping_detail', [
                'staff' => $staff,
                'week'=>$week,
                'date'=>$date,
                'keepingTime'=>$keepingTime,
                'year'=>$year,
                'month'=>$month
            ]);
        }
        return redirect()->back();
    }
    public function PostKeepingStaff( Request $request){
        $staff=session('admin');
        if($staff){
            $StaffId=$staff->StaffId;
            $date=$this->getDate();
            $month = $request->month;
            $year = $request->year;
            $week=$this->getWeek($month, $year);
            $staff=Staff::where('StaffId',$StaffId)->first();
            $keepingTime=KeepingTime::where('StaffId',$StaffId)
                ->whereMonth('KeepingTime',$month)->get();
            return view('staff/staff_keeping_detail', [
                'staff' => $staff,
                'week'=>$week,
                'date'=>$date,
                'keepingTime'=>$keepingTime,
                'year'=>$year,
                'month'=>$month
            ]);
        }
        return redirect()->back();
    }
    public function PostKeepingReason(Request $request){
        $KeepingTimeId=$request->get('KeepingTimeId');
        DB::table('KeepingTime')->where('KeepingTimeId', $KeepingTimeId)->update([
            'Note' => StringHelper::convertUtf8($request->get('Note'))
        ]);
        return redirect()->back()->with('success','Cập nhật thành công');
    }
    public static function getPermission(){
        $user=session()->get('admin');
        $permission_parent = UserPermission::join('Permission','UserPermission.PermissionId','=','Permission.PermissionId')
            ->where('UserPermission.StaffId', $user->StaffId)
            ->select('Permission.ParentId')->distinct()->get();
        foreach ($permission_parent as $key=>$item){
            $parent=Permission::where('PermissionId',$item->ParentId)->first();
            $permission_parent[$key]->name=$parent->Name;
            $permission_parent[$key]->icon=$parent->Icon;
            $permission_parent[$key]->link=$parent->Link;
            $permission = UserPermission::join('Permission','UserPermission.PermissionId','=','Permission.PermissionId')
                ->where('UserPermission.StaffId', $user->StaffId)
                ->where('ParentId',$item->ParentId)
                ->select('Permission.*')->get();
            $permission_parent[$key]->permission=$permission;
        }
//        dd($permission_parent->toArray());
        return view('partial.permission',['permission'=>$permission_parent]);
    }
    public function PostLogin(Request $request){
        try{
            $rules = [
                'staffId' => 'required',
                'password' => 'required',
            ];
            $messages = [
                'staffId.required' => 'Mã nhân viên không được để trống',
                'password.required' => 'Mật khẩu không được để trống',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails())
                return redirect()->back()->withInput()->withErrors($validator);
            $user=Staff::where('StaffId',$request->staffId)->first();
            if($user){
                if(Hash::check($request->password,$user->Password)){
                    if($user->IsActive){
                        session(['admin'=> $user]);
                        return redirect('/');
                    }else{
                        return redirect()->back()->withInput()->with('error', 'Tài khoản này đã bị khoá, vui lòng liên hệ quản trị viên để biết thêm chi tiết!');
                    }
                }else{
                    return redirect()->back()->withInput()->with('error', 'Bạn đã nhập sai thông tin đăng nhập, vui lòng thử lại');
                }
            }else{
                return redirect()->back()->withInput()->with('error', 'Bạn đã nhập sai thông tin đăng nhập, vui lòng thử lại');
            }
        }catch (\Exception $e){
            dd($e);
        }
    }

    //quản lí
    public function ListUserAdmin()
    {
        $user = Staff::all()->sortByDesc('created_at');
        if ($user) {
            return view('useradmin/index', [
                'user' => $user
            ]);
        }
    }
    public function Create()
    {
        $permission = Permission::where('ParentId',0)->orderBy('PermissionId')->get();
        foreach ($permission as $key=>$p){
            $child=Permission::where('ParentId',$p->PermissionId)->orderBy('Name')->get();
            $permission[$key]->child=$child;
        }
        dd($permission->toArray());
        $staff=Staff::all()->sortByDesc('StaffId');
        return view('useradmin/create',[
            'staff'=>$staff,
            'permission'=>$permission
        ]);
    }

    public function Storage(Request $request)
    {
        try{
            if (empty($request->get('Password')))
                return redirect()->back()->with('error','Vui lòng không để trống mật khẩu')->withInput();

            $pass = trim($request->get('Password'));
            if (strlen($pass) < 8)
                return redirect()->back()->with('error','Mật khẩu ít nhất là 8 ký tự')->withInput();
            DB::table('Staff')->where('StaffId', $request->get('StaffId'))->update([
                'IsActive' => $request->get('IsActive'),
                'Password'=> $pass
            ]);
            $result=DB::transaction(function () use ($request){
                if(count($request->Permission)){
                    foreach ($request->Permission as $PermissionId){
                        $userPermission=new UserPermission();
                        $userPermission->PermissionId=$PermissionId;
                        $userPermission->StaffId=$request->get('StaffId');
                        $userPermission->save();
                    }
                }
                return redirect('/quan-tri/danh-sach')->with('success','Thêm thành công');
            });
            return $result;
//            if ($cardtype->save()) {
//                return redirect('/quan-tri')->with('success','Thêm thành công');
//            }
        }catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('error','Thêm thất bại');
        }

    }

    public function Edit($UserAdminId)
    {
        try{
            $staff=Staff::where('StaffId', $UserAdminId)->first();
//            $user = UserAdmin::where('UserAdminId', $UserAdminId)->first();
            if ($staff) {
                $arr=UserPermission::where('StaffId',$UserAdminId)->pluck('PermissionId')->toArray();
//                dd($arr);
                $permission = Permission::where('ParentId',0)->orderBy('PermissionId')->get();
                foreach ($permission as $key=>$p){
                    $child=Permission::where('ParentId',$p->PermissionId)->orderBy('Name')->get();
                    foreach ($child as $k=>$st){
                        if(in_array($st->PermissionId,$arr)){
                            $child[$k]->selected=1;
                        }else{
                            $child[$k]->selected=0;
                        }
                    }
                    $permission[$key]->child=$child;
                }
                return view('useradmin/edit', [
                    'staff'=>$staff,
                    'permission'=>$permission
                ]);
            }else{
                return redirect()->back()->with('error','Quản trị không tồn tại');
            }
        }catch (\Exception $e){
            dd($e);
            return redirect()->back();
        }
    }

    public function Update(Request $request, $UserAdminId)
    {
        try{
            if (empty($request->get('Password'))){

                $result=DB::transaction(function () use ( $request, $UserAdminId){
                    DB::table('Staff')->where('StaffId', $UserAdminId)->update([
                        'IsActive' => $request->get('IsActive')
                    ]);
                    $event = UserPermission::where('StaffId', $UserAdminId)->delete();
                    if(count($request->Permission)){

                        foreach ($request->Permission as $PermissionId){
                            $userPermission=new UserPermission();
                            $userPermission->PermissionId=$PermissionId;
                            $userPermission->StaffId=$UserAdminId;
                            $userPermission->save();
                        }
                    }
                    return redirect()->back()->with('success','Cập nhật thành công');
                });
                return $result;
            }else{
                $pass = trim($request->get('Password'));
                if (strlen($pass) < 8)
                    return redirect()->back()->with('error','Mật khẩu ít nhất là 8 ký tự')->withInput();

                $result=DB::transaction(function () use ( $request, $UserAdminId, $pass){
                    DB::table('Staff')->where('StaffId', $UserAdminId)->update([
                        'IsActive' => $request->get('IsActive'),
                        'Password' => bcrypt($pass)
                    ]);
                    $event = UserPermission::where('StaffId', $UserAdminId)->delete();

                    if(count($request->Permission)){

                        foreach ($request->Permission as $PermissionId){
                            $userPermission=new UserPermission();
                            $userPermission->PermissionId=$PermissionId;
                            $userPermission->StaffId=$UserAdminId;
                            $userPermission->save();
                        }
                    }
                    return redirect('/quan-tri/danh-sach')->with('success','Cập nhật thành công');
                });
                return $result;
            }
        }catch (\Exception $e){
            dd($e);
            return redirect('/quan-tri/danh-sach')->with('error','Cập nhật thất bại');
        }

    }

    public function Delete($UserAdminId)
    {
        try{
            DB::table('Staff')->where('StaffId', $UserAdminId)->update([
                'IsActive' => 0
            ]);
            return redirect('/quan-tri/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/quan-tri/danh-sach')->with('error','Xóa thất bại');
        }
    }
    public function Profile(){
        try{
            $staff=session('admin');
            if ($staff) {
                $role=Role::all()->sortByDesc('RoleId');
                $department = Department::all()->sortByDesc('DepartmentId');
                $stafftype = StaffType::all()->sortByDesc('StaffTypeId');
                $staffCard=Staff::where('MifareCardId','!=',$staff->MifareCardId)->select('MifareCardId')->get();
                $arr=[];
                foreach ($staffCard as $item){
                    if($item->MifareCardId)
                        $arr[]=$item->MifareCardId;
                }
                $card = MifareCard::join('MifareCardType','MifareCard.MifareCardTypeId','=','MifareCardType.MifareCardTypeId')
                    ->whereNotIn('MifareCard.MifareCardId',$arr)
                    ->orderByDesc('MifareCard.MifareCardId')
                    ->select('MifareCard.*','MifareCardType.MifareCardTypeName')
                    ->distinct()
                    ->get();
                return view('useradmin/profile', [
                    'staff'=>$staff,
                    'role'=>$role,
                    'department'=>$department,
                    'stafftype'=>$stafftype,
                    'card'=>$card
                ]);
            }else{
                return view('useradmin/profile', ['staff'=>null]);
            }
        }catch (\Exception $e){

        }

    }
    public function formatDate($date){
        return Carbon::createFromFormat('d/m/Y',$date)->format('Y-m-d');
    }
    public function UpdateProfile(Request $request){
        try{
            $user=session('admin');
            DB::table('Staff')->where('StaffId', $user->StaffId)->update([
                'StaffName' => StringHelper::convertUtf8($request->get('StaffName')),
                'MifareCardId' => StringHelper::convertUtf8($request->get('MifareCardId')),
                'Birthday' => $request->get('Birthday')? $this->formatDate($request->get('Birthday')): NULL,
                'Gender' => $request->get('Gender'),
                'IdentityCard' => StringHelper::convertUtf8($request->get('IdentityCard')),
                'DateOfIssue' => $request->get('DateOfIssue')? $this->formatDate($request->get('DateOfIssue')): NULL,
                'PlaceOfIssue' => StringHelper::convertUtf8($request->get('PlaceOfIssue')),
                'SocialInsuranceNo' => StringHelper::convertUtf8($request->get('SocialInsuranceNo')),
                'RoleId' => $request->get('RoleId'),
                'StaffTypeId' => $request->get('StaffTypeId'),
                'DepartmentId' =>  $request->get('DepartmentId'),
                'ScaleGroupId' => StringHelper::convertUtf8($request->get('ScaleGroupId')),
                'ScaleId' => StringHelper::convertUtf8($request->get('ScaleId')),
                'Level' => $request->get('Level'),
                'Rate' => $request->get('Rate'),
                'ResponsibilityPayment' => $request->get('ResponsibilityPayment'),
                'SeniorityAllowance' => $request->get('SeniorityAllowance'),
                'EducationLevel' => StringHelper::convertUtf8($request->get('EducationLevel')),
                'Degree' => StringHelper::convertUtf8($request->get('Degree')),
                'MajorSubject' => StringHelper::convertUtf8($request->get('MajorSubject')),
                'ManagementLevel' => StringHelper::convertUtf8($request->get('ManagementLevel')),
                'PoliticLevel' => StringHelper::convertUtf8($request->get('PoliticLevel')),
                'ForeignerLanguageLevel' => StringHelper::convertUtf8($request->get('ForeignerLanguageLevel')),
                'ComputerSkillLevel' => StringHelper::convertUtf8($request->get('ComputerSkillLevel')),
                'Nation' => StringHelper::convertUtf8($request->get('Nation')),
                'Religion' => StringHelper::convertUtf8($request->get('Religion')),
                'IsAdherer' => $request->get('IsAdherer'),
                'AdherentDate' => $request->get('AdherentDate')? $this->formatDate($request->get('AdherentDate')):NULL,
                'OfficalAdherentDate' => $request->get('OfficalAdherentDate')? $this->formatDate($request->get('OfficalAdherentDate')):NULL,
                'PlaceOfBirth' => StringHelper::convertUtf8($request->get('PlaceOfBirth')),
                'HomeTown' => StringHelper::convertUtf8($request->get('HomeTown')),
                'CurrentAddress' => StringHelper::convertUtf8($request->get('CurrentAddress')),
                'WorkingDate' => $request->get('WorkingDate')? $this->formatDate($request->get('WorkingDate')):NULL,
                'IsReview' => 0,
                'updated_at' => Carbon::now(),
                'Note' => StringHelper::convertUtf8($request->get('Note')),
            ]);
            return redirect('/quan-tri/thong-tin')->with('success','Cập nhật thành công');
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function ChangePass(){
        return view('useradmin.changepass');
    }
    public function PostChangePass(Request $request){
        try{
            $rules = [
                'Password' => 'required|min:8',
                'PasswordNew' => 'required|min:8',
                'PasswordNewCf' => 'required|same:PasswordNew'
            ];
            $messages = [
                'Password.required' => 'Mật khẩu hiện tại không được để trống',
                'PasswordNew.required' => 'Mật khẩu mới không được để trống',
                'PasswordNew.min' => 'Mật khẩu mới phải lớn hơn 8 ký tự',
                'PasswordNewCf.required' => 'Mật khẩu xác nhận không được trống',
                'PasswordNewCf.same' => 'Xác nhận mật khẩu mới không trùng khớp',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails())
                return redirect()->back()->withInput()->withErrors($validator);
            $user=session('admin');
            $pass = trim($request->PasswordNew);
            if(Hash::check($request->Password,$user->Password)){
                DB::table('UserAdmin')->where('UserAdminId', $user->StaffId)->update([
                    'Password' => bcrypt($pass)
                ]);
                return redirect()->back()->withInput()->with('success', 'Cập nhật mật khẩu thành công');
            }else{
                return redirect()->back()->withInput()->with('error', 'Mật khẩu hiện tại không đúng');
            }
        }catch (\Exception $e){

        }

    }
    public function ListLeaveOfAbsence()
    {
        $leave = LeaveOfAbsence::where('StaffId',session()->get('admin')->StaffId)->orderByDesc('created_at')->get();
        if ($leave) {
            return view('useradmin/index-leave', [
                'leave' => $leave
            ]);
        }
    }
    public function getLeaveOfAbsence(){
        return view('useradmin.leave-of-absence');
    }
    public function postLeaveOfAbsence(Request $request){
        try{
            $leave = new LeaveOfAbsence();
            $leave->Reason = StringHelper::convertUtf8($request->get('Reason'));
            $leave->TotalDate = StringHelper::convertUtf8($request->get('TotalDate'));
            $leave->StaffId = session()->get('admin')->StaffId;
            $leave->DepartmentId = session()->get('admin')->DepartmentId;
            $leave->DateStart = Carbon::createFromFormat('d/m/Y',$request->get('DateStart'))->format('Y-m-d');
            $leave->DateEnd = Carbon::createFromFormat('d/m/Y',$request->get('DateEnd'))->format('Y-m-d');
            $leave->Status = 0;
            $leave->save();
            return redirect('/don-nghi/danh-sach')->with('success','Thêm thành công');
        }catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('error','Thêm thất bại');
        }
    }
    public function EditLeaveOfAbsence($LeaveOfAbsenceId){
        try{
            $leave=LeaveOfAbsence::where('LeaveOfAbsenceId',$LeaveOfAbsenceId)->first();
            return view('useradmin.edit-leave-of-absence',[
                'leave'=>$leave
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with('error','Thất bại');
        }

    }
    public function UpdateLeaveOfAbsence($LeaveOfAbsenceId, Request $request){
        try{

                DB::table('LeaveOfAbsence')->where('LeaveOfAbsenceId', $LeaveOfAbsenceId)->update([
                    'Reason' => StringHelper::convertUtf8($request->get('Reason')),
                    'TotalDate' => StringHelper::convertUtf8($request->get('TotalDate')),
                    'DateStart' =>Carbon::createFromFormat('d/m/Y',$request->get('DateStart'))->format('Y-m-d'),
                    'DateEnd' => Carbon::createFromFormat('d/m/Y',$request->get('DateEnd'))->format('Y-m-d'),
                ]);
                return redirect('/don-nghi/danh-sach')->with('success','Cập nhật thành công');

        }catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('error','Cập nhật thất bại');
        }

    }
    public function DeleteLeaveOfAbsence($LeaveOfAbsenceId)
    {
        try{
            $leave = LeaveOfAbsence::where('LeaveOfAbsenceId', $LeaveOfAbsenceId)->delete();
            return redirect('/don-nghi/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/don-nghi/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
