<?php
/**
 * Created by PhpStorm.
 * User: BSTAR
 * Date: 5/3/2018
 * Time: 3:43 PM
 */

namespace App\Http\Controllers;

use App\Models\MifareCardType;
use App\Models\Staff;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function ListofUser(){
        try{
            $staff=Staff::get();
            return view('welcome',[
                'staff'=>$staff
            ]);
        }catch (\Exception $e){
            dd($e);
        }
    }
    public function Create(Request $request){
        $staff = new MifareCardType();
        $staff->MifareCardTypeName= 'Demo card';
        $staff->isActive= 1;
        $staff->save();
        return redirect('/');
    }
}