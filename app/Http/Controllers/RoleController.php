<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    //
    public function ListRole()
    {
        $role = Role::all()->sortByDesc('RoleId');
        if ($role) {
            return view('role/index', [
                'role' => $role
            ]);
        }
    }
    public function Create()
    {
        return view('role/create');
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('RoleName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên vai trò')->withInput();
        $role = new Role();
        $role->RoleName = StringHelper::convertUtf8($request->get('RoleName'));
//        $cardtype->IsActive = $request->get('IsActive');
        if ($role->save()) {
            return redirect('/vai-tro/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($RoleId)
    {
        $role = Role::where('RoleId', $RoleId)->first();
        if ($role) {
            return view('role/edit', [
                'role' => $role
            ]);
        }

    }

    public function Update(Request $request, $RoleId)
    {
        if (empty($request->get('RoleName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên vai trò')->withInput();

        DB::table('Role')->where('RoleId', $RoleId)->update([
            'RoleName' => StringHelper::convertUtf8($request->get('RoleName'))

        ]);
//        Session::flash('message','Cập nhật thành công');
        return redirect('/vai-tro/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($RoleId)
    {
        try{
            $role = Role::where('RoleId', $RoleId)->delete();
            return redirect('/vai-tro/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/vai-tro/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
