<?php
/**
 * Created by PhpStorm.
 * User: BSTAR
 * Date: 5/4/2018
 * Time: 10:14 AM
 */

namespace App\Http\Controllers;


use App\Common\StringHelper;
use App\Models\Department;
use App\Models\Event;
use App\Models\EventStaff;
use App\Models\KeepingTime;
use App\Models\LeaveOfAbsence;
use App\Models\Staff;
use App\Models\StaffScore;
use App\Models\StaffType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    public function Index()
    {
//        $department = Department::all()->sortByDesc('DepartmentId');
//        if ($department) {
//            return view('welcome', [
//                'department' => $department
//            ]);
//        }
        $staff=session('admin');
        if($staff){
            $staff=Staff::where('StaffId',$staff->StaffId)->first();
            $date1=KeepingTime::where('StaffId',$staff->StaffId)->whereMonth('CheckOutTime','=',Carbon::now()->month)->where('Type',1)->count();
            $date2=KeepingTime::where('StaffId',$staff->StaffId)->whereMonth('CheckOutTime','=',Carbon::now()->month)->where('isLate',1)->count();
            $date3=30-8-$date1;
            $date4=KeepingTime::where('StaffId',$staff->StaffId)->whereMonth('CheckOutTime','=',Carbon::now()->month)->where('isLate',0)->count();
            $score=StaffScore::where('StaffId',$staff->StaffId)->where('Month',Carbon::now()->month)->where('Year',Carbon::now()->year)->first();
            $score_total=StaffScore::where('StaffId',$staff->StaffId)->sum('Score');
            return view('useradmin.profile-home',[
                'staff'=>$staff,
                'date1'=>$date1,
                'date2'=>$date2,
                'date3'=>$date3,
                'date4'=>$date4,
                'score'=>$score,
                'score_total'=>$score_total

            ]);
        }

    }
    public static function countNotification(){
        $noti=Staff::where('IsReview',0)->count();
        return $noti;
    }
    public static function ListStaff(){
        $count=Staff::where('IsReview',0)->count();
        $staff=Staff::where('IsReview',0)->limit(5)->get();
        return view('partial/liNo', [
            'staff' => $staff,
            'count'=>$count
        ]);
    }
    public static function countEvent(){
        $event=EventStaff::where('StaffId',session('admin')->StaffId)
            ->where('Status',1)
            ->count();
        return $event;
    }
    public static function ListEvent(){
        $event = Event::join('EventStaff','Event.EventId','=','EventStaff.EventId')
            ->where('EventStaff.StaffId',session('admin')->StaffId)
            ->where('EventStaff.Status',1)->orderByDesc('updated_at')->select('Event.*')->limit(5)->get();
        if(count($event)!=0){
            foreach ($event as $key=>$e){
                $count=EventStaff::where('EventId',$e->EventId)->count();
                $event[$key]->count=$count;
            }
        }
        return view('partial/liEvent', [
            'event' => $event
        ]);
    }


//đơn xin nghỉ
    public static function checkDerpartment(){
        $staff=session()->get('admin');
        $type=StaffType::where('StaffTypeId',$staff->StaffTypeId)
            ->where('Type',1)->exists();
        return $type;
    }

    public function ListLeaveOfAbsence(){
        try{
            $staff=session()->get('admin');
            $type=StaffType::where('StaffTypeId',$staff->StaffTypeId)
                ->where('Type',1)->exists();
            if($type){
                $leave=LeaveOfAbsence::where('LeaveOfAbsence.DepartmentId',$staff->DepartmentId)
                    ->leftjoin('Staff','LeaveOfAbsence.StaffId','=','Staff.StaffId')
                    ->select('LeaveOfAbsence.*','Staff.StaffName')
                    ->get();
                return view('department.index-leave',['leave'=>$leave]);
            }else{
                return redirect()->back();
            }
        }catch (\Exception $e){
            return redirect()->back();
        }
    }
    public function EditLeaveOfAbsence($LeaveOfAbsenceId){
        try{
            $leave=LeaveOfAbsence::where('LeaveOfAbsenceId',$LeaveOfAbsenceId)->first();
            $staff=Staff::where('StaffId',$leave->StaffId)->first();
            return view('department.edit-leave-of-absence',[
                'leave'=>$leave,
                'staff'=>$staff
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with('error','Thất bại');
        }

    }
    public function UpdateLeaveOfAbsence($LeaveOfAbsenceId, Request $request){
        try{

            DB::table('LeaveOfAbsence')->where('LeaveOfAbsenceId', $LeaveOfAbsenceId)->update([
                'Status' => $request->Status ? 1: 0
            ]);
            return redirect('/phong-ban/don-xin-nghi')->with('success','Cập nhật thành công');

        }catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('error','Cập nhật thất bại');
        }

    }


    //Quản lý phòng ban
    public function ListDepartment()
    {
        $department = Department::all()->sortByDesc('DepartmentId');
        if ($department) {
            return view('department/index', [
                'department' => $department
            ]);
        }
    }

    public function Create()
    {
        return view('department/create');
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('DepartmentName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên phòng')->withInput();
        if (empty($request->get('BaseDepartmentId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã phòng')->withInput();
        if (empty($request->get('StartTime')))
            return redirect()->back()->with('error','Vui lòng không để trống thời gian bắt dầu làm việc')->withInput();
        if (empty($request->get('DelayTime')))
            return redirect()->back()->with('error','Vui lòng không để trống thời gian trì hoãn')->withInput();
        $department = new Department();
        $department->DepartmentName = StringHelper::convertUtf8($request->get('DepartmentName'));
        $department->BaseDepartmentId = StringHelper::convertUtf8($request->get('BaseDepartmentId'));
        $department->StartTime = $request->get('StartTime');
        $department->DelayTime = $request->get('DelayTime');
        if ($department->save()) {
            return redirect('/phong-ban/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($DepartmentId)
    {
        $department = Department::where('DepartmentId', $DepartmentId)->first();
        if ($department) {
            return view('department/edit', [
                'department' => $department
            ]);
        }

    }

    public function Update(Request $request, $DepartmentId)
    {
        if (empty($request->get('DepartmentName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên phòng')->withInput();
        if (empty($request->get('BaseDepartmentId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã phòng')->withInput();
        DB::table('Department')->where('DepartmentId', $DepartmentId)->update([
            'DepartmentName' => StringHelper::convertUtf8($request->get('DepartmentName')),
            'BaseDepartmentId' => StringHelper::convertUtf8($request->get('BaseDepartmentId'))
        ]);
        return redirect('/phong-ban/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($DepartmentId)
    {
        try{
            $department = Department::where('DepartmentId', $DepartmentId)->delete();
            return redirect('/phong-ban/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/phong-ban/danh-sach')->with('error','Xóa thất bại');
        }
    }

    public function ListDepartmentKeeping()
    {
        $department = Department::all()->sortByDesc('DepartmentId');
        if ($department) {
            return view('department/index_keeping', [
                'department' => $department
            ]);
        }
    }
    public function ListStaffByDepartment($DepartmentId)
    {
        $date=$this->getDate();
        $now = Carbon::now();
        $month = $now->month;
        $year = $now->year;
        $week=$this->getWeek($month, $year);
        $department=Department::where('DepartmentId',$DepartmentId)->first();
        $staff= Staff::where('DepartmentId',$DepartmentId)->paginate(10);
        foreach ($staff as $key=>$t){
            $staff[$key]->worktime = $this->getWorkTimeMonth($t->StaffId, $month, $year);
        }
        return view('department/staff_keeping', [
            'department' => $department,
            'staff'=>$staff,
            'week'=>$week,
            'date'=>$date,
            'year'=>$year,
            'month'=>$month
        ]);
    }
    public function ListStaffByDepartmentMonth($DepartmentId, Request $request){
        $date=$this->getDate();
        $now = Carbon::now();
        $month = $request->month;
        $year = $request->year;
        $week=$this->getWeek($month, $year);
        $department=Department::where('DepartmentId',$DepartmentId)->first();
        $staff= Staff::where('DepartmentId',$DepartmentId)->paginate(10);
        foreach ($staff as $key=>$t){
            $staff[$key]->worktime = $this->getWorkTimeMonth($t->StaffId, $month, $year);
        }
        return view('department/staff_keeping', [
            'department' => $department,
            'staff'=>$staff,
            'week'=>$week,
            'date'=>$date,
            'year'=>$year,
            'month'=>$month
        ]);
    }
    public function getWorkTimeMonth($StaffId, $month, $year){
        $keepingTime=KeepingTime::where('StaffId',$StaffId)
            ->whereMonth('KeepingTime',$month)
            ->whereYear('KeepingTime',$year)
            ->count();
        return $keepingTime;
    }
    public function getDate(){
        $date=[
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
        ];
        return $date;
    }
    public function getWeek($month, $year){
        $list=array();
        $week=array();
        $i=0;
        for($d=1; $d<=31; $d++)
        {
            $time=mktime(12, 0, 0, $month, $d, $year);
            if (date('m', $time)==$month){
                $list[]=date('Y-m-d-D', $time);
                $week[$i][date('D', $time)]=date('Y-m-d', $time);
                if(date('D', $time)=='Sat'){
                    $i++;
                }
            }
        }
        return $week;
    }
    public function KeepingStaff($StaffId){
        $date=$this->getDate();
        $now = Carbon::now();
        $month = $now->month;
        $year = $now->year;
        $week=$this->getWeek($month, $year);
        $staff=Staff::where('StaffId',$StaffId)->first();
        $keepingTime= $keepingTime=KeepingTime::where('StaffId',$StaffId)
            ->whereMonth('KeepingTime',$month)
            ->whereYear('KeepingTime',$year)->get();
        return view('department/staff_keeping_detail', [
            'staff' => $staff,
            'week'=>$week,
            'date'=>$date,
            'keepingTime'=>$keepingTime,
            'year'=>$year,
            'month'=>$month
        ]);
    }
    public function PostKeepingStaff($StaffId, Request $request){
        $date=$this->getDate();
        $month = $request->month;
        $year = $request->year;
        $week=$this->getWeek($month, $year);
        $staff=Staff::where('StaffId',$StaffId)->first();
        $keepingTime=KeepingTime::where('StaffId',$StaffId)
            ->whereMonth('KeepingTime',$month)->get();
        return view('department/staff_keeping_detail', [
            'staff' => $staff,
            'week'=>$week,
            'date'=>$date,
            'keepingTime'=>$keepingTime,
            'year'=>$year,
            'month'=>$month
        ]);
    }
    public function PostKeepingReason(Request $request){
        $StaffId=$request->get('StaffId');
        $KeepingTimeId=$request->get('KeepingTimeId');
        DB::table('KeepingTime')->where('KeepingTimeId', $KeepingTimeId)->update([
            'Note' => StringHelper::convertUtf8($request->get('Note'))
        ]);
        return redirect('/thong-ke/cham-cong/staff/'.$StaffId)->with('success','Cập nhật thành công');
    }
}