<?php

namespace App\Http\Controllers;

use App\Common\StringHelper;
use App\Models\DayOffInWeek;
use App\Models\Holiday;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigDayOffController extends Controller
{
    //
    public function ListDayOffInWeek(){
        $list=DayOffInWeek::all();
        return view('config_day.day_off', [
            'dayOff' => $list
        ]);
    }
    public function PostDayOffInWeek(Request $request){
        $arr=$request->all();
        DB::table('DayOffInWeek')->where('Status', 1)->update([
            'Status' => 0
        ]);
        foreach ($arr as $key=>$value){
            if($key != '_token'){
                DB::table('DayOffInWeek')->where('DayOffInWeekId', $key)->update([
                    'Status' => 1
                ]);
            }

        }
        return redirect()->back()->with('success','Cập nhật thành công!');
    }
    public function ListHoliday(){
        $holiday=Holiday::all();

        return view('config_day.index', [
            'holiday'=>$holiday
        ]);
    }
    public function formatDate($date){
        try{
            return Carbon::createFromFormat('d/m/Y',$date)->format('Y-m-d');
        }catch (\Exception $e){
            return NULL;
        }

    }
    public function postHoliday(Request $request){
        $id = $request->get('Id');
        if(empty($id)){
            $holiday = new Holiday();
            $holiday->Date = $request->get('Date')? $this->formatDate($request->get('Date')): NULL;
            $holiday->Note = StringHelper::convertUtf8($request->get('Note'));
            if ($holiday->save()) {
                return redirect()->back()->with('success','Thêm thành công');
            }
        }else{
            DB::table('Holiday')->where('HolidayId', $id)->update([
                'Date' => $request->get('Date')? $this->formatDate($request->get('Date')): NULL,
                'Note' => StringHelper::convertUtf8($request->get('Note'))
            ]);
            return redirect()->back()->with('success','Cập nhật thành công');
        }
    }
    public function deleteHoliday($HolidayId){
        try{
            $Holiday = Holiday::where('HolidayId', $HolidayId)->delete();
            return redirect()->back()->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect()->back()->with('error','Xóa thất bại');
        }
    }
}
