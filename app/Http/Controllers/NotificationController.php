<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Event;
use App\Models\EventStaff;
use App\Models\MifareCard;
use App\Models\Role;
use App\Models\Staff;
use App\Models\StaffType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    //
    public static function ListEvent(){
        $event = Event::join('EventStaff','Event.EventId','=','EventStaff.EventId')
            ->where('EventStaff.StaffId',session('admin')->StaffId)
            ->orderByDesc('updated_at')->select('Event.*','EventStaff.Status')->get();
        if(count($event)!=0){
            foreach ($event as $key=>$e){
                $count=EventStaff::where('EventId',$e->EventId)->count();
                $event[$key]->count=$count;
            }
        }
        return view('notification/event', [
            'event' => $event
        ]);
    }
    public function EventDetail($EventId)
    {
//        $event = Event::where('EventId', $EventId)->first();
        $event = Event::join('EventStaff','Event.EventId','=','EventStaff.EventId')
            ->where('Event.EventId', $EventId)
            ->where('EventStaff.StaffId',session('admin')->StaffId)
            ->orderByDesc('updated_at')
            ->select('Event.*','EventStaff.Status')
            ->first();
        $eventStaff=EventStaff::where('EventId',$EventId)
            ->join('Staff','EventStaff.StaffId','=','Staff.StaffId')
            ->leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
            ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
            ->orderByDesc('Staff.updated_at')
            ->select('Staff.StaffId','Staff.StaffName',
                'StaffType.StaffTypeName','Department.DepartmentName','EventStaff.Status','EventStaff.ProposeByStaffId')
            ->get();

        $staffPropose=EventStaff::where('EventId',$EventId)
            ->where('EventStaff.ProposeByStaffId',session('admin')->StaffId)
            ->join('Staff','EventStaff.StaffId','=','Staff.StaffId')
            ->leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
            ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
            ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
            ->orderByDesc('Staff.updated_at')
            ->select('Staff.StaffId','Staff.StaffName','Staff.MifareCardId',
                'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName','EventStaff.Status')
            ->first();
        if ($event) {
            return view('notification/eventDetail', [
                'event' => $event,
                'staff'=>$eventStaff,
                'staffPropose'=>$staffPropose
            ]);
        }else{

            return redirect('/thong-bao/su-kien')->with('error','Xem thất bại');
        }
    }
    public function EventAccept($EventId){
        try{
            DB::table('EventStaff')->where('EventId', $EventId)->where('StaffId',session('admin')->StaffId)->update([
               'Status'=>2
            ]);
            return redirect()->back()->with('success','Chấp nhận thành công!');
        }catch (\Exception $e){
            return redirect()->back()->with('error','Xem thất bại');
        }
    }
    public function ProposeStaff($EventId){
        $event = Event::where('EventId', $EventId)->first();
        $staff = Staff::leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
            ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
            ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
            ->where('Staff.StaffId','!=',session('admin')->StaffId)
            ->whereNotIn('Staff.StaffId', function ($q) use ($EventId){
                $q->select('StaffId')->from('EventStaff')->where('EventId',$EventId);
            })
            ->orderByDesc('Staff.updated_at')
            ->select('Staff.StaffId','Staff.StaffName','Staff.MifareCardId',
                'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName')
            ->get();
        return view('notification/listStaffPropose',[
            'staff'=>$staff,
            'event'=>$event
        ]);
    }
    public function postProposeStaff($EventId, Request $request){
        try{
//            dd($request->all());
            $result=DB::transaction(function () use($request, $EventId){
                DB::table('EventStaff')->where('EventId', $EventId)->where('StaffId',session('admin')->StaffId)->update([
                    'Status'=>0
                ]);
                $eventStaff=new EventStaff();
                $eventStaff->EventId=$EventId;
                $eventStaff->StaffId=$request->StaffId;
                $eventStaff->Status=1;
                $eventStaff->ProposeByStaffId=session('admin')->StaffId;
                $eventStaff->save();


                return redirect('/thong-bao/su-kien/'.$EventId)->with('success','Đề xuất thành công');
            });
            return $result;
        }catch (\Exception $e){
            dd($e);
            return redirect('/thong-bao/su-kien/'.$EventId)->with('error','Đề xuất thất bại!');
        }
    }










//    public static function ListStaff(){
//        $staff=Staff::where('IsReview',0)->limit(5)->get();
//        return view('notification/liNo', [
//            'staff' => $staff
//        ]);
//    }
    public function ListStaff()
        {
            $role = Staff::leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
                ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
                ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
                ->orderBy('Staff.IsReview')
                ->select('Staff.StaffId','Staff.StaffName','Staff.MifareCardId',
                    'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName','Staff.IsReview')
                ->get();
            return view('notification/staff', [
                'staff' => $role
            ]);

        }
    public function StaffDetail($StaffId)
    {
        $role=Role::all()->sortByDesc('RoleId');
        $department = Department::all()->sortByDesc('DepartmentId');
        $stafftype = StaffType::all()->sortByDesc('StaffTypeId');

        $staff = Staff::where('StaffId', $StaffId)->first();
        if ($staff) {
            $staffCard=Staff::where('MifareCardId','!=',$staff->MifareCardId)->select('MifareCardId')->get();
            $arr=[];
            foreach ($staffCard as $item){
                if($item->MifareCardId)
                    $arr[]=$item->MifareCardId;
            }
            $card = MifareCard::join('MifareCardType','MifareCard.MifareCardTypeId','=','MifareCardType.MifareCardTypeId')
                ->whereNotIn('MifareCard.MifareCardId',$arr)
                ->orderByDesc('MifareCard.MifareCardId')
                ->select('MifareCard.*','MifareCardType.MifareCardTypeName')
                ->distinct()
                ->get();
            return view('notification/staffDetail', [
                'staff'=>$staff,
                'role'=>$role,
                'department'=>$department,
                'stafftype'=>$stafftype,
                'card'=>$card
            ]);
        }

    }
}
