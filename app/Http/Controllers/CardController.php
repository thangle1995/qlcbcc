<?php

namespace App\Http\Controllers;

use App\Models\MifareCard;
use App\Models\MifareCardType;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CardController extends Controller
{
    //
    public function ListCard()
    {
        $card = MifareCard::join('MifareCardType','MifareCard.MifareCardTypeId','=','MifareCardType.MifareCardTypeId')
            ->orderByDesc('MifareCard.MifareCardId')
            ->select('MifareCard.*','MifareCardType.MifareCardTypeName')
            ->get();
        if ($card) {
            return view('card/index', [
                'card' => $card
            ]);
        }
    }
    public function Create()
    {
        $cardtype=MifareCardType::all()->sortByDesc('MifareCardTypeId');
        return view('card/create',[
            'cardtype'=>$cardtype
        ]);
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('MifareCardId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã thẻ')->withInput();
        if (empty($request->get('Serial')))
            return redirect()->back()->with('error','Vui lòng không để trống số serial')->withInput();
        $card = new MifareCard();
        $card->MifareCardId = StringHelper::convertUtf8($request->get('MifareCardId'));
        $card->Serial = StringHelper::convertUtf8($request->get('Serial'));
        $card->MifareCardTypeId = $request->get('MifareCardTypeId');
        $card->IsActive = $request->get('IsActive');
        if ($card->save()) {
            return redirect('/the/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($MifareCardId)
    {
        $cardtype=MifareCardType::all()->sortByDesc('MifareCardTypeId');
        $card = MifareCard::where('MifareCardId', $MifareCardId)->first();
        if ($card) {
            return view('card/edit', [
                'card' => $card,
                'cardtype'=>$cardtype
            ]);
        }
    }

    public function Update(Request $request, $MifareCardId)
    {
        if (empty($request->get('MifareCardId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã thẻ')->withInput();
        if (empty($request->get('Serial')))
            return redirect()->back()->with('error','Vui lòng không để trống số serial')->withInput();

        DB::table('MifareCard')->where('MifareCardId', $MifareCardId)->update([
            'MifareCardId' => StringHelper::convertUtf8($request->get('MifareCardId')),
            'Serial' => StringHelper::convertUtf8($request->get('Serial')),
            'MifareCardTypeId' => $request->get('MifareCardTypeId'),
            'IsActive' => $request->get('IsActive')
        ]);
//        Session::flash('message','Cập nhật thành công');
        return redirect('/the/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($MifareCardId)
    {
        try{
            $card = MifareCard::where('MifareCardId', $MifareCardId)->delete();
            return redirect('/the/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/the/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
