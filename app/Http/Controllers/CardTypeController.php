<?php

namespace App\Http\Controllers;

use App\Models\MifareCardType;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CardTypeController extends Controller
{
    //
    public function ListCardType()
    {
        $cardtype = MifareCardType::all()->sortByDesc('MifareCardTypeId');
        if ($cardtype) {
            return view('cardtype/index', [
                'cardtype' => $cardtype
            ]);
        }
    }
    public function Create()
    {
        return view('cardtype/create');
    }

    public function Storage(Request $request)
    {
        if (empty($request->get('MifareCardTypeName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên loại thẻ')->withInput();
        $cardtype = new MifareCardType();
        $cardtype->MifareCardTypeName = StringHelper::convertUtf8($request->get('MifareCardTypeName'));
        $cardtype->IsActive = $request->get('IsActive');
        if ($cardtype->save()) {
            return redirect('/loai-the/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($MifareCardTypeId)
    {
        $cardtype = MifareCardType::where('MifareCardTypeId', $MifareCardTypeId)->first();
        if ($cardtype) {
            return view('cardtype/edit', [
                'cardtype' => $cardtype
            ]);
        }

    }

    public function Update(Request $request, $MifareCardTypeId)
    {
        if (empty($request->get('MifareCardTypeName')))
            return redirect()->back()->with('error','Vui lòng không để trống tên loại thẻ')->withInput();

        DB::table('MifareCardType')->where('MifareCardTypeId', $MifareCardTypeId)->update([
            'MifareCardTypeName' => StringHelper::convertUtf8($request->get('MifareCardTypeName')),
            'IsActive' => $request->get('IsActive')
        ]);
//        Session::flash('message','Cập nhật thành công');
        return redirect('/loai-the/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($MifareCardTypeId)
    {
        try{
            $cardtype = MifareCardType::where('MifareCardTypeId', $MifareCardTypeId)->delete();
            return redirect('/loai-the/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){
            return redirect('/loai-the/danh-sach')->with('error','Xóa thất bại');
        }
    }
}
