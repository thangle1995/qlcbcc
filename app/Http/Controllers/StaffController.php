<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\MifareCard;
use App\Models\Role;
use App\Models\Staff;
use App\Models\StaffType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    //

    public function ListStaff()
    {
        $role = Staff::leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
            ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
            ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
            ->orderByDesc('Staff.updated_at')
            ->select('Staff.StaffId','Staff.StaffName','Staff.MifareCardId',
                'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName','Staff.IsReview')
            ->get();
        $department=Department::get();
        return view('staff/index', [
            'staff' => $role,
            'department'=>$department
        ]);

    }
    public function ListStaffByDepartment($DepartmentId){
        $role = Staff::leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
            ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
            ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
            ->where('Staff.DepartmentId',$DepartmentId)
            ->orderByDesc('Staff.updated_at')
            ->select('Staff.StaffId','Staff.StaffName','Staff.MifareCardId',
                'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName','Staff.IsReview')
            ->get();
        $department=Department::get();
        return view('staff/index', [
            'staff' => $role,
            'department'=>$department
        ]);
    }
    public static function getStaff($StaffId){
        $staff = Staff::where('StaffId', $StaffId)->select('StaffId','StaffName')->first();
        return $staff;
    }
    public function Create()
    {
        $role=Role::all()->sortByDesc('RoleId');
        $department = Department::all()->sortByDesc('DepartmentId');
        $stafftype = StaffType::all()->sortByDesc('StaffTypeId');
        $staffCard=Staff::select('MifareCardId')->get();
        $arr=[];
        foreach ($staffCard as $item){
            if($item->MifareCardId)
                $arr[]=$item->MifareCardId;
        }
        $card = MifareCard::join('MifareCardType','MifareCard.MifareCardTypeId','=','MifareCardType.MifareCardTypeId')
            ->whereNotIn('MifareCard.MifareCardId',$arr)
            ->orderByDesc('MifareCard.MifareCardId')
            ->select('MifareCard.*','MifareCardType.MifareCardTypeName')
            ->distinct()
            ->get();
        return view('staff/create',[
            'role'=>$role,
            'department'=>$department,
            'stafftype'=>$stafftype,
            'card'=>$card
        ]);
    }
    public function formatDate($date){
        try{
            return Carbon::createFromFormat('d/m/Y',$date)->format('Y-m-d');
        }catch (\Exception $e){
            return NULL;
        }

    }
    public function Storage(Request $request)
    {
        if (empty($request->get('StaffId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã nhân viên')->withInput();
        $staff = new Staff();
        $staff->StaffId = StringHelper::convertUtf8($request->get('StaffId'));
        $staff->Password = $request->get('StaffId');
        $staff->IsActive = 1;
        $staff->MifareCardId = StringHelper::convertUtf8($request->get('MifareCardId'));
        $staff->StaffName = StringHelper::convertUtf8($request->get('StaffName'));
        $staff->Birthday = $request->get('Birthday')? $this->formatDate($request->get('Birthday')): NULL;
        $staff->Gender = $request->get('Gender');
        $staff->IdentityCard = StringHelper::convertUtf8($request->get('IdentityCard'));
        $staff->DateOfIssue = $request->get('DateOfIssue')? $this->formatDate($request->get('DateOfIssue')): NULL;
        $staff->PlaceOfIssue = StringHelper::convertUtf8($request->get('PlaceOfIssue'));
        $staff->SocialInsuranceNo = StringHelper::convertUtf8($request->get('SocialInsuranceNo'));
        $staff->RoleId = $request->get('RoleId');
        $staff->StaffTypeId = $request->get('StaffTypeId');
        $staff->DepartmentId = $request->get('DepartmentId');
        $staff->ScaleGroupId = StringHelper::convertUtf8($request->get('ScaleGroupId'));
        $staff->ScaleId = StringHelper::convertUtf8($request->get('ScaleId'));
        $staff->Level = $request->get('Level');
        $staff->Rate = $request->get('Rate');
        $staff->ResponsibilityPayment = $request->get('ResponsibilityPayment');
        $staff->SeniorityAllowance = $request->get('SeniorityAllowance');

        $staff->EducationLevel = StringHelper::convertUtf8($request->get('EducationLevel'));
        $staff->Degree = StringHelper::convertUtf8($request->get('Degree'));
        $staff->MajorSubject = StringHelper::convertUtf8($request->get('MajorSubject'));
        $staff->ManagementLevel = StringHelper::convertUtf8($request->get('ManagementLevel'));
        $staff->PoliticLevel = StringHelper::convertUtf8($request->get('PoliticLevel'));
        $staff->ForeignerLanguageLevel = StringHelper::convertUtf8($request->get('ForeignerLanguageLevel'));
        $staff->ComputerSkillLevel = StringHelper::convertUtf8($request->get('ComputerSkillLevel'));
        $staff->Nation = StringHelper::convertUtf8($request->get('Nation'));
        $staff->Religion = StringHelper::convertUtf8($request->get('Religion'));
        $staff->IsAdherer = $request->get('IsAdherer');
        $staff->AdherentDate = $request->get('AdherentDate')? $this->formatDate($request->get('AdherentDate')):NULL;
        $staff->OfficalAdherentDate = $request->get('OfficalAdherentDate')? $this->formatDate($request->get('OfficalAdherentDate')):NULL;
        $staff->PlaceOfBirth = StringHelper::convertUtf8($request->get('PlaceOfBirth'));
        $staff->HomeTown = StringHelper::convertUtf8($request->get('HomeTown'));
        $staff->CurrentAddress = StringHelper::convertUtf8($request->get('CurrentAddress'));
        $staff->WorkingDate = $request->get('WorkingDate')? $this->formatDate($request->get('WorkingDate')):NULL;
        $staff->Note = StringHelper::convertUtf8($request->get('Note'));
        $staff->IsReview = $request->get('IsReview');

//        $cardtype->IsActive = $request->get('IsActive');
        if ($staff->save()) {
            return redirect('/nhan-vien/danh-sach')->with('success','Thêm thành công');
        }
    }

    public function Edit($StaffId)
    {
        $role=Role::all()->sortByDesc('RoleId');
        $department = Department::all()->sortByDesc('DepartmentId');
        $stafftype = StaffType::all()->sortByDesc('StaffTypeId');

        $staff = Staff::where('StaffId', $StaffId)->first();
        if ($staff) {
            $staffCard=Staff::where('MifareCardId','!=',$staff->MifareCardId)->select('MifareCardId')->get();
            $arr=[];
            foreach ($staffCard as $item){
                if($item->MifareCardId)
                    $arr[]=$item->MifareCardId;
            }
            $card = MifareCard::join('MifareCardType','MifareCard.MifareCardTypeId','=','MifareCardType.MifareCardTypeId')
                ->whereNotIn('MifareCard.MifareCardId',$arr)
                ->orderByDesc('MifareCard.MifareCardId')
                ->select('MifareCard.*','MifareCardType.MifareCardTypeName')
                ->distinct()
                ->get();
            return view('staff/edit', [
                'staff'=>$staff,
                'role'=>$role,
                'department'=>$department,
                'stafftype'=>$stafftype,
                'card'=>$card
            ]);
        }

    }

    public function Update(Request $request, $StaffId)
    {
        if (empty($request->get('StaffId')))
            return redirect()->back()->with('error','Vui lòng không để trống mã nhân viên')->withInput();
        $staff=Staff::where('StaffId','!=',$StaffId)
            ->where('StaffId',$request->get('StaffId'))
            ->exists();
        if($staff){
            return redirect()->back()->with('error','Mã nhân viên đã tồn tại')->withInput();
        }
        DB::table('Staff')->where('StaffId', $StaffId)->update([
            'StaffId' => StringHelper::convertUtf8($request->get('StaffId')),
            'MifareCardId'=> StringHelper::convertUtf8($request->get('MifareCardId')),
            'StaffName' => StringHelper::convertUtf8($request->get('StaffName')),
            'Birthday' => $request->get('Birthday')? $this->formatDate($request->get('Birthday')): NULL,
            'Gender' => $request->get('Gender'),
            'IdentityCard' => StringHelper::convertUtf8($request->get('IdentityCard')),
            'DateOfIssue' => $request->get('DateOfIssue')? $this->formatDate($request->get('DateOfIssue')): NULL,
            'PlaceOfIssue' => StringHelper::convertUtf8($request->get('PlaceOfIssue')),
            'SocialInsuranceNo' => StringHelper::convertUtf8($request->get('SocialInsuranceNo')),
            'RoleId' => $request->get('RoleId'),
            'StaffTypeId' => $request->get('StaffTypeId'),
            'DepartmentId' =>  $request->get('DepartmentId'),
            'ScaleGroupId' => StringHelper::convertUtf8($request->get('ScaleGroupId')),
            'ScaleId' => StringHelper::convertUtf8($request->get('ScaleId')),
            'Level' => $request->get('Level'),
            'Rate' => $request->get('Rate'),
            'ResponsibilityPayment' => $request->get('ResponsibilityPayment'),
            'SeniorityAllowance' => $request->get('SeniorityAllowance'),
            'EducationLevel' => StringHelper::convertUtf8($request->get('EducationLevel')),
            'Degree' => StringHelper::convertUtf8($request->get('Degree')),
            'MajorSubject' => StringHelper::convertUtf8($request->get('MajorSubject')),
            'ManagementLevel' => StringHelper::convertUtf8($request->get('ManagementLevel')),
            'PoliticLevel' => StringHelper::convertUtf8($request->get('PoliticLevel')),
            'ForeignerLanguageLevel' => StringHelper::convertUtf8($request->get('ForeignerLanguageLevel')),
            'ComputerSkillLevel' => StringHelper::convertUtf8($request->get('ComputerSkillLevel')),
            'Nation' => StringHelper::convertUtf8($request->get('Nation')),
            'Religion' => StringHelper::convertUtf8($request->get('Religion')),
            'IsAdherer' => $request->get('IsAdherer'),
            'AdherentDate' => $request->get('AdherentDate')? $this->formatDate($request->get('AdherentDate')):NULL,
            'OfficalAdherentDate' => $request->get('OfficalAdherentDate')? $this->formatDate($request->get('OfficalAdherentDate')):NULL,
            'PlaceOfBirth' => StringHelper::convertUtf8($request->get('PlaceOfBirth')),
            'HomeTown' => StringHelper::convertUtf8($request->get('HomeTown')),
            'CurrentAddress' => StringHelper::convertUtf8($request->get('CurrentAddress')),
            'WorkingDate' => $request->get('WorkingDate')? $this->formatDate($request->get('WorkingDate')):NULL,
            'Note' => StringHelper::convertUtf8($request->get('Note')),
            'IsReview' => $request->get('IsReview'),
            'updated_at' => Carbon::now(),
        ]);
        $link=$request->btnSave;
        return redirect('/nhan-vien/danh-sach')->with('success','Cập nhật thành công');
    }

    public function Delete($StaffId)
    {
        try{
            $role = Staff::where('StaffId', $StaffId)->delete();
            return redirect('/nhan-vien/danh-sach')->with('success','Xóa thành công');
        }catch (\Exception $ex){


            dd($ex);
            return redirect('/nhan-vien/danh-sach')->with('error','Xóa thất bại');
        }
    }
    public function ExportStaff(){
        try{
            $style = array(
                'alignment' => array(
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            $phpFont = new \PHPExcel_Style_Font();
//            $phpFont = new PHPExcel_Style_Font();
            $phpFont->setBold(false);
            $phpFont->setName('Times New Roman');
            $phpFont->setSize(12);

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getDefaultStyle()->applyFromArray($style);
            $objPHPExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
            $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getDefaultStyle()->setFont($phpFont);

            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("15");
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("15");
            //TpHCM
            $objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'THÀNH PHỐ HỒ CHÍ MINH');

            //DANH SÁCH VIÊN CHỨC
            $objPHPExcel->getActiveSheet()->mergeCells('C4:AD4');
            $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'DANH SÁCH VIÊN CHỨC');
            //STT
            $objPHPExcel->getActiveSheet()->mergeCells('A6:A8');
            $objPHPExcel->getActiveSheet()->SetCellValue('A6', 'STT');
            //Mã nhân viên
            $objPHPExcel->getActiveSheet()->mergeCells('B6:B8');
            $objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Mã nhân viên');
            //Họ và tên
            $objPHPExcel->getActiveSheet()->mergeCells('C6:C8');
            $objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Họ và tên');
            //Ngày tháng năm sinh
            $objPHPExcel->getActiveSheet()->mergeCells('D6:D8');
            $objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Ngày tháng năm sinh');
            //Giới tính
            $objPHPExcel->getActiveSheet()->mergeCells('E6:E8');
            $objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Giới tính');
            //CMND
            $objPHPExcel->getActiveSheet()->mergeCells('F6:F8');
            $objPHPExcel->getActiveSheet()->SetCellValue('F6', 'CMND');
            //Ngày cấp
            $objPHPExcel->getActiveSheet()->mergeCells('G6:G8');
            $objPHPExcel->getActiveSheet()->SetCellValue('G6', 'Ngày cấp');
            //Số BHXH
            $objPHPExcel->getActiveSheet()->mergeCells('H6:H8');
            $objPHPExcel->getActiveSheet()->SetCellValue('H6', 'Số BHXH');
            //Chức vụ
            $objPHPExcel->getActiveSheet()->mergeCells('I6:I8');
            $objPHPExcel->getActiveSheet()->SetCellValue('I6', 'Chức vụ');
            //PL theo lĩnh vực
            $objPHPExcel->getActiveSheet()->mergeCells('J6:J8');
            $objPHPExcel->getActiveSheet()->SetCellValue('J6', 'PL theo lĩnh vực');
            //Loại
            $objPHPExcel->getActiveSheet()->mergeCells('K6:K8');
            $objPHPExcel->getActiveSheet()->SetCellValue('K6', 'Loại');
            //Mã đơn vị
            $objPHPExcel->getActiveSheet()->mergeCells('L6:L8');
            $objPHPExcel->getActiveSheet()->SetCellValue('L6', 'Mã đơn vị');
            //Đơn vị công tác
            $objPHPExcel->getActiveSheet()->mergeCells('M6:M8');
            $objPHPExcel->getActiveSheet()->SetCellValue('M6', 'Đơn vị công tác');
            //Nhóm ngạch
            $objPHPExcel->getActiveSheet()->mergeCells('N6:N8');
            $objPHPExcel->getActiveSheet()->SetCellValue('N6', 'Nhóm ngạch');
            //Mã số ngạch
            $objPHPExcel->getActiveSheet()->mergeCells('O6:O8');
            $objPHPExcel->getActiveSheet()->SetCellValue('O6', 'Mã số ngạch');
            //Bậc, hệ số lương hiện hưởng
            $objPHPExcel->getActiveSheet()->mergeCells('P6:U6');
            $objPHPExcel->getActiveSheet()->SetCellValue('P6', 'Bậc, hệ số lương hiện hưởng');
            //Bậc
            $objPHPExcel->getActiveSheet()->mergeCells('P7:P8');
            $objPHPExcel->getActiveSheet()->SetCellValue('P7', 'Bậc');
            //Hệ số
            $objPHPExcel->getActiveSheet()->mergeCells('Q7:Q8');
            $objPHPExcel->getActiveSheet()->SetCellValue('Q7', 'Hệ số');
            //PC chức vụ
            $objPHPExcel->getActiveSheet()->mergeCells('R7:R8');
            $objPHPExcel->getActiveSheet()->SetCellValue('R7', 'PC chức vụ');
            //PC thâm niên VK
            $objPHPExcel->getActiveSheet()->mergeCells('S7:S8');
            $objPHPExcel->getActiveSheet()->SetCellValue('S7', 'PC thâm niên VK');
            //CL bảo lưu
            $objPHPExcel->getActiveSheet()->mergeCells('T7:T8');
            $objPHPExcel->getActiveSheet()->SetCellValue('T7', 'CL bảo lưu');
            //Mốc thời gian nâng lương lần sau
            $objPHPExcel->getActiveSheet()->mergeCells('U7:U8');
            $objPHPExcel->getActiveSheet()->SetCellValue('U7', 'Mốc thời gian nâng lương lần sau');
            //Trình độ
            $objPHPExcel->getActiveSheet()->mergeCells('V6:AB6');
            $objPHPExcel->getActiveSheet()->SetCellValue('V6', 'Trình độ');
            //Văn hóa
            $objPHPExcel->getActiveSheet()->mergeCells('V7:V8');
            $objPHPExcel->getActiveSheet()->SetCellValue('V7', 'Văn hóa');
            //Chuyên môn
            $objPHPExcel->getActiveSheet()->mergeCells('W7:W8');
            $objPHPExcel->getActiveSheet()->SetCellValue('W7', 'Chuyên môn');
            //Chuyên ngành
            $objPHPExcel->getActiveSheet()->mergeCells('X7:X8');
            $objPHPExcel->getActiveSheet()->SetCellValue('X7', 'Chuyên ngành');
            //QLNN
            $objPHPExcel->getActiveSheet()->mergeCells('Y7:Y8');
            $objPHPExcel->getActiveSheet()->SetCellValue('Y7', 'QLNN');
            //Chính trị
            $objPHPExcel->getActiveSheet()->mergeCells('Z7:Z8');
            $objPHPExcel->getActiveSheet()->SetCellValue('Z7', 'Chính trị');
            //Ngoại ngữ
            $objPHPExcel->getActiveSheet()->mergeCells('AA7:AA8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AA7', 'Ngoại ngữ');
            //Tin học
            $objPHPExcel->getActiveSheet()->mergeCells('AB7:AB8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AB7', 'Tin học');
            //Dân tộc
            $objPHPExcel->getActiveSheet()->mergeCells('AC6:AC8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AC6', 'Dân tộc');
            //Tôn giáo
            $objPHPExcel->getActiveSheet()->mergeCells('AD6:AD8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AD6', 'Tôn giáo');
            //Đảng viên
            $objPHPExcel->getActiveSheet()->mergeCells('AE6:AE8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AE6', 'Đảng viên');
            //Nơi sinh
            $objPHPExcel->getActiveSheet()->mergeCells('AF6:AF8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AF6', 'Nơi sinh');
            //Quê quán
            $objPHPExcel->getActiveSheet()->mergeCells('AG6:AG8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AG6', 'Quê quán');
            //Địa chỉ
            $objPHPExcel->getActiveSheet()->mergeCells('AH6:AH8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AH6', 'Địa chỉ');
            //Ngày Tham gia công tác
            $objPHPExcel->getActiveSheet()->mergeCells('AI6:AI8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AI6', 'Ngày Tham gia công tác');
            //Ghi chú
            $objPHPExcel->getActiveSheet()->mergeCells('AJ6:AJ8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AJ6', 'Ghi chú');
            //Ngày vào Đảng
            $objPHPExcel->getActiveSheet()->mergeCells('AK6:AK8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AK6', 'Ngày vào Đảng');
            //Ngày vào Đảng (chính thức)
            $objPHPExcel->getActiveSheet()->mergeCells('AL6:AL8');
            $objPHPExcel->getActiveSheet()->SetCellValue('AL6', 'Ngày vào Đảng (chính thức)');
            $phpFont1 = new \PHPExcel_Style_Font();
            $phpFont1->setBold(false);
            $phpFont1->setItalic(true);
            $phpFont1->setSize(10);
            $col_arr = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'
                ,'Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH',
                'AI','AJ','AK','AL'];
            foreach ($col_arr as $key=>$cl){
                $objPHPExcel->getActiveSheet()->SetCellValue($cl.'9', $key+1);
            }
            $objPHPExcel->getActiveSheet()->getStyle('A6:AL9')->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyle('A9:AL9')->getFont()->setItalic(true);
            $objPHPExcel->getActiveSheet()->getStyle('A9:AL9')->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle('A9:AL9')->getFont()->setBold(false);

            $objPHPExcel->getActiveSheet()->getStyle('A1:AL8')->getFont()->setBold(true);
            $staff = Staff::leftjoin('StaffType','StaffType.StaffTypeId','=','Staff.StaffTypeId')
                ->leftjoin('Role','Role.RoleId','=','Staff.RoleId')
                ->leftjoin('Department','Department.DepartmentId','=','Staff.DepartmentId')
                ->orderByDesc('Staff.updated_at')
                ->select('Staff.*','Staff.MifareCardId',
                    'StaffType.StaffTypeName','Role.RoleName','Department.DepartmentName','Department.BaseDepartmentId')
                ->get()->sortByDesc('Staff.StaffName');
            $rowCount = 10;
            $index=1;
            foreach ($staff as $item){
                $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $index);
                $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $item['StaffId']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $item['StaffName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, Carbon::parse($item['Birthday'])->format('d/m/Y'));
                if($item['Gender']==1)
                    $gender='Nam';
                else
                    $gender="Nữ";
                $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $gender);
                $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $item['IdentityCard']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, Carbon::parse($item['DateOfIssue'])->format('d/m/Y'));
                $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $item['SocialInsuranceNo']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $item['StaffTypeName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "");
                $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $item['RoleName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $item['BaseDepartmentId']);
                $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $item['DepartmentName']);
                $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $item['ScaleGroupId']);
                $objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $item['ScaleId']);
                $objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $item['Level']);
                $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $item['Rate']);
                $objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $item['ResponsibilityPayment']);
                $objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $item['SeniorityAllowance']);
                $objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, '');
                $objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, '');
                $objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $item['EducationLevel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('W'.$rowCount, $item['Degree']);
                $objPHPExcel->getActiveSheet()->SetCellValue('X'.$rowCount, $item['MajorSubject']);
                $objPHPExcel->getActiveSheet()->SetCellValue('Y'.$rowCount, $item['ManagementLevel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('Z'.$rowCount, $item['PoliticLevel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AA'.$rowCount, $item['ForeignerLanguageLevel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AB'.$rowCount, $item['ComputerSkillLevel']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AC'.$rowCount, $item['Nation']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AD'.$rowCount, $item['Religion']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AE'.$rowCount, $item['IsAdherer'] ? 'X':'');
                $objPHPExcel->getActiveSheet()->SetCellValue('AF'.$rowCount, $item['PlaceOfBirth']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AG'.$rowCount, $item['HomeTown']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AH'.$rowCount, $item['CurrentAddress']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AI'.$rowCount, Carbon::parse($item['WorkingDate'])->format('d/m/Y'));
                $objPHPExcel->getActiveSheet()->SetCellValue('AJ'.$rowCount, $item['Note']);
                $objPHPExcel->getActiveSheet()->SetCellValue('AK'.$rowCount, Carbon::parse($item['AdherentDate'])->format('d/m/Y'));
                $objPHPExcel->getActiveSheet()->SetCellValue('AL'.$rowCount, Carbon::parse($item['OfficalAdherentDate'])->format('d/m/Y'));
                $objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount.':AL'.$rowCount)->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $rowCount++;
                $index++;

            }


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Limesurvey_Results.xls"');
            header('Cache-Control: max-age=0');
            $objWriter =\PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            return redirect()->back();
        }catch (\Exception $e){
            dd($e);
        }

    }

    function genNameFile($ext = 'jpg')
    {
        return uniqid(time()) . '.' . $ext;
    }
    function MoveFile($img,$filePath = 'uploads/product_image',$data_type_accept = array('gif','png' ,'jpg','bmp','jpeg'))
    {
        try{
            if(!is_file($img)){
                return [
                    "success"=>false,
                    "message"=>"File lỗi"
                ];
            }

            $ext = $img->getClientOriginalExtension();
            if(!in_array(strtolower($ext),$data_type_accept))
                return [
                    "success"=>false,
                    "message"=>"Định dạng file không hỗ trợ, định dạng cho phép: " . implode(',',$data_type_accept)
                ];

            $filename_source = $img->getClientOriginalName();
            $filename = $filename_source . '_' . $this->genNameFile($ext);
            if($img->move($filePath, $filename))
                return [
                    "success"=>true,
                    "file_name"=>$filename,
                    "full_path"=>"$filePath/$filename"
                ];

//            return [
//                "success"=>false,
//                "message"=>"Lỗi không upload được file"
//            ];
        }catch (\Exception $e){
            dd($e);
        }

    }
    function get_file_extension($file_name)
    {
        return substr(strrchr($file_name, '.'), 1);
    }
    function fromstaffBoard($path)
    {
        if(!file_exists($path))
            return null;
        if($this->get_file_extension($path) == "xls")
            $objReader = new \PHPExcel_Reader_Excel5();

        else
            $objReader = new \PHPExcel_Reader_Excel2007();

        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($path);

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

        $sheet = $objPHPExcel->getActiveSheet();
        $array_data = array();
        $col_arr = ['B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'
            ,'Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH',
            'AI','AJ','AK','AL'];
        foreach($rowIterator as $row)
        {

            $rowIndex = $row->getRowIndex ();

            if($rowIndex < 10) continue;
            $staff_code_col = $sheet->getCell('B' . $rowIndex);
            $staff_code = trim($staff_code_col->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[0].$rowIndex);
            $array_data[$staff_code]["StaffId"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[1].$rowIndex);
            $array_data[$staff_code]["StaffName"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[2].$rowIndex);
//            dd($this->formatDate(trim($cell->getCalculatedValue())));
//            $d=trim($cell->getCalculatedValue());
            $array_data[$staff_code]["Birthday"] = $this->formatDate(trim($cell->getCalculatedValue()));

            $cell = $sheet->getCell($col_arr[3].$rowIndex);
            if(trim($cell->getCalculatedValue())=="Nam")
                $array_data[$staff_code]["Gender"] = 1;
            else
                $array_data[$staff_code]["Gender"] = 2;

            $cell = $sheet->getCell($col_arr[4].$rowIndex);
            $array_data[$staff_code]["IdentityCard"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[5].$rowIndex);
            $d=trim($cell->getCalculatedValue());
            $array_data[$staff_code]["DateOfIssue"] = $this->formatDate($d);

//            $cell = $sheet->getCell($col_arr[6].$rowIndex);
//            $array_data[$staff_code]["PlaceOfIssue"] = trim($cell->getCalculatedValue());
            $cell = $sheet->getCell($col_arr[6].$rowIndex);
            $array_data[$staff_code]["SocialInsuranceNo"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[7].$rowIndex);
            $array_data[$staff_code]["StaffTypeId"] = $this->getStaffType(trim($cell->getCalculatedValue()));

//            $cell = $sheet->getCell($col_arr[8].$rowIndex);
//            $array_data[$staff_code]["commune"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[9].$rowIndex);
            $array_data[$staff_code]["RoleId"] = $this->getRole(trim($cell->getCalculatedValue()));

            $cell = $sheet->getCell($col_arr[10].$rowIndex);
            $array_data[$staff_code]["DepartmentId"] = $this->getDepartment(trim($cell->getCalculatedValue()));

//            $cell = $sheet->getCell($col_arr[11].$rowIndex);
//            $array_data[$staff_code]["household"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[12].$rowIndex);
            $array_data[$staff_code]["ScaleGroupId"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[13].$rowIndex);
            $array_data[$staff_code]["ScaleId"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[14].$rowIndex);
            $array_data[$staff_code]["Level"] = trim($cell->getCalculatedValue())?trim($cell->getCalculatedValue()):0;

            $cell = $sheet->getCell($col_arr[15].$rowIndex);
            $array_data[$staff_code]["Rate"] = trim($cell->getCalculatedValue()) ? trim($cell->getCalculatedValue()): 0;

            $cell = $sheet->getCell($col_arr[16].$rowIndex);
            $array_data[$staff_code]["ResponsibilityPayment"] = trim($cell->getCalculatedValue())?trim($cell->getCalculatedValue()):0;

            $cell = $sheet->getCell($col_arr[17].$rowIndex);
            $array_data[$staff_code]["SeniorityAllowance"] = trim($cell->getCalculatedValue())?trim($cell->getCalculatedValue()):0;


//            $cell = $sheet->getCell($col_arr[18].$rowIndex);
//            $array_data[$staff_code]["household"] = trim($cell->getCalculatedValue());
//
//            $cell = $sheet->getCell($col_arr[19].$rowIndex);
//            $array_data[$staff_code]["household"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[20].$rowIndex);
            $array_data[$staff_code]["EducationLevel"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[21].$rowIndex);
            $array_data[$staff_code]["Degree"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[22].$rowIndex);
            $array_data[$staff_code]["MajorSubject"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[23].$rowIndex);
            $array_data[$staff_code]["ManagementLevel"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[24].$rowIndex);
            $array_data[$staff_code]["PoliticLevel"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[25].$rowIndex);
            $array_data[$staff_code]["ForeignerLanguageLevel"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[26].$rowIndex);
            $array_data[$staff_code]["ComputerSkillLevel"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[27].$rowIndex);
            $array_data[$staff_code]["Nation"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[28].$rowIndex);
            $array_data[$staff_code]["Religion"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[29].$rowIndex);
            if(trim($cell->getCalculatedValue())=="X"){
                $array_data[$staff_code]["IsAdherer"] = 1;
            }
            else{
                $array_data[$staff_code]["IsAdherer"] = 0;
            }
            $cell = $sheet->getCell($col_arr[30].$rowIndex);
            $array_data[$staff_code]["PlaceOfBirth"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[31].$rowIndex);
            $array_data[$staff_code]["HomeTown"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[32].$rowIndex);
            $array_data[$staff_code]["CurrentAddress"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[33].$rowIndex);
            $array_data[$staff_code]["WorkingDate"] = $this->formatDate(trim($cell->getCalculatedValue()));

            $cell = $sheet->getCell($col_arr[34].$rowIndex);
            $array_data[$staff_code]["Note"] = trim($cell->getCalculatedValue());

            $cell = $sheet->getCell($col_arr[35].$rowIndex);
            $array_data[$staff_code]["AdherentDate"] = $this->formatDate(trim($cell->getCalculatedValue()));

            $cell = $sheet->getCell($col_arr[36].$rowIndex);
            $array_data[$staff_code]["OfficalAdherentDate"] = $this->formatDate(trim($cell->getCalculatedValue()));


        }
        return $array_data;
    }
    public function getRole($RoleName){
        $role=Role::where('RoleName',$RoleName)->first();
        if($role)
            return $role->RoleId;
        return "";
    }
    public function getStaffType($StaffTypeName){
        $role=StaffType::where('StaffTypeName',$StaffTypeName)->first();
        if($role)
            return $role->StaffTypeId;
        return "";
    }
    public function getDepartment($BaseDepartmentId){
        $role=Department::where('BaseDepartmentId',$BaseDepartmentId)->first();
        if($role)
            return $role->DepartmentId;
        return "";
    }
    public function postImportStaffExcel(Request $request)
    {
        try{
            $file = $request->file('file');
            $result = $this->MoveFile($file, 'uploads', ['xls', 'xlsx']);
            $fromstaffBoard = $this->fromstaffBoard($result['full_path']);
//        dd($fromstaffBoard);
            if (!$result['success'])
                return redirect()->back()->withInput()->with('error', $result['message']);

            if(count($fromstaffBoard)!=0)
            {
                foreach ($fromstaffBoard as $key => $value) {
                    $check=Staff::where('StaffId',$value['StaffId'])->exists();
                    if($check)
                        return redirect()->back()->with('error','Mã nhân viên '.$value['StaffId'].' đã tồn tại!')->withInput();
                }
                foreach ($fromstaffBoard as $key => $value) {
                    $staff = new Staff();
                    $staff->StaffId = StringHelper::convertUtf8($value['StaffId']);
                    $staff->MifareCardId = NULL;
                    $staff->StaffName = StringHelper::convertUtf8($value['StaffName']);
                    $staff->Birthday = $value['Birthday'];
                    $staff->Gender = $value['Gender'];
                    $staff->IdentityCard = StringHelper::convertUtf8($value['IdentityCard']);
                    $staff->DateOfIssue = $value['DateOfIssue'];
                    $staff->PlaceOfIssue = '';
                    $staff->SocialInsuranceNo = StringHelper::convertUtf8($value['SocialInsuranceNo']);
                    $staff->RoleId = $value['RoleId'];
                    $staff->StaffTypeId = $value['StaffTypeId'];
                    $staff->DepartmentId = $value['DepartmentId'];
                    $staff->ScaleGroupId = StringHelper::convertUtf8($value['ScaleGroupId']);
                    $staff->ScaleId = StringHelper::convertUtf8($value['ScaleId']);
                    $staff->Level = $value['Level'];
                    $staff->Rate = $value['Rate'];
                    $staff->ResponsibilityPayment = $value['ResponsibilityPayment'];
                    $staff->SeniorityAllowance = $value['SeniorityAllowance'];

                    $staff->EducationLevel = StringHelper::convertUtf8($value['EducationLevel']);
                    $staff->Degree = StringHelper::convertUtf8($value['Degree']);
                    $staff->MajorSubject = StringHelper::convertUtf8($value['MajorSubject']);
                    $staff->ManagementLevel = StringHelper::convertUtf8($value['ManagementLevel']);
                    $staff->PoliticLevel = StringHelper::convertUtf8($value['PoliticLevel']);
                    $staff->ForeignerLanguageLevel = StringHelper::convertUtf8($value['ForeignerLanguageLevel']);
                    $staff->ComputerSkillLevel = StringHelper::convertUtf8($value['ComputerSkillLevel']);
                    $staff->Nation = StringHelper::convertUtf8($value['Nation']);
                    $staff->Religion = StringHelper::convertUtf8($value['Religion']);
                    $staff->IsAdherer = $value['IsAdherer'];
                    $staff->AdherentDate = $value['AdherentDate'];
                    $staff->OfficalAdherentDate = $value['OfficalAdherentDate'];
                    $staff->PlaceOfBirth = StringHelper::convertUtf8($value['PlaceOfBirth']);
                    $staff->HomeTown = StringHelper::convertUtf8($value['HomeTown']);
                    $staff->CurrentAddress = StringHelper::convertUtf8($value['CurrentAddress']);
                    $staff->WorkingDate = $value['WorkingDate'];
                    $staff->IsReview = 1;
//                    dd($staff);
                    $staff->save();
                }
            }
            return redirect()->back()->with('success', 'Đã nhập thành công!');
        }catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('error', 'Đã xảy ra lỗi!');
        }

    }
}
