<?php

namespace App\Http\Controllers;

use App\Jobs\SendConfirmMail;
use App\Models\Department;
use App\Models\Event;
use App\Models\EventStaff;
use App\Models\KeepingTime;
use App\Models\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Common\StringHelper;
use Illuminate\Support\Facades\DB;

class eventController extends Controller
{
    //
    public function ListEvent()
    {
        $event = Event::all()->sortByDesc('updated_at');
        if (count($event) != 0) {
            foreach ($event as $key => $e) {
                $count = EventStaff::where('EventId', $e->EventId)->count();
                $event[$key]->count = $count;
            }
        }
        if ($event) {
            return view('event/index', [
                'event' => $event
            ]);
        }
    }

    public function ListEventReport()
    {
        $event = Event::all()->sortByDesc('updated_at');
        if (count($event) != 0) {
            foreach ($event as $key => $e) {
                $count = EventStaff::where('EventId', $e->EventId)->count();
                $event[$key]->count = $count;
            }
        }
        if ($event) {
            return view('event/index-report-news', [
                'event' => $event
            ]);
        }
    }

    public function Create()
    {
        $department = Department::orderBy('DepartmentName')->select('DepartmentId', 'DepartmentName')->get();
        if (count($department) != 0) {
            foreach ($department as $key => $item) {
                $staff = Staff::leftjoin('StaffType', 'StaffType.StaffTypeId', '=', 'Staff.StaffTypeId')
                    ->where('DepartmentId', $item->DepartmentId)
                    ->orderByDesc('Staff.updated_at')
                    ->select('Staff.StaffId', 'Staff.StaffName',
                        'StaffType.StaffTypeName')
                    ->get();
                $department[$key]->staff = $staff;
            }
        }
        return view('event/create', [
            'department' => $department
        ]);
    }

    public function formatDate($date)
    {
        try {
            return Carbon::createFromFormat('d/m/Y H:s', $date)->format('Y-m-d H:s');
        } catch (\Exception $e) {
            return '';
        }
    }

    public function eventConfirm($eventStaffId, $token)
    {
        $user = Staff::where('Token', $token)->first();
        if ($user) {
            $eventStaff = EventStaff::where('EventStaffId', $eventStaffId)
                ->where('StaffId', $user->StaffId)->first();
            if ($eventStaff) {
                $event = Event::where('EventId', $eventStaff->EventId)->first();
                if ($eventStaff->Status == 1) {
                    DB::table('EventStaff')->where('EventStaffId', $eventStaffId)->update([
                        'Status' => 2
                    ]);
                }
                return view('event.confirm', ['event' => $event]);
            }
        }
        return view('event.confirm', ['event' => null]);
    }

    public function sendMail($event, $event_staff)
    {
        $user = Staff::where('StaffId', $event_staff->StaffId)->first();
        if ($user) {
            if (!empty($user->Email)) {
                $token = encrypt($user->StaffId . $user->Email . time());
                $email = $user->Email;
                $staff_name = $user->StaffName;
                $this->dispatch(new SendConfirmMail($email,$staff_name,$event->Title, $event->StartDate, $event->EndDate, $event->Address, $event->Description, $event_staff->EventStaffId, $token) );
                DB::table('Staff')->where('StaffId', $user->StaffId)->update([
                    'Token' => $token
                ]);
            }
        }
        return false;
    }

    public function Storage(Request $request)
    {
        try {
            if (empty($request->get('Title')))
                return redirect()->back()->with('error', 'Vui lòng không để trống tên sự kiện')->withInput();
            if (empty($request->get('StartDate')))
                return redirect()->back()->with('error', 'Vui lòng không để trống thời gian bắt đầu sự kiện')->withInput();
            if (empty($request->get('EndDate')))
                return redirect()->back()->with('error', 'Vui lòng không để trống thời gian kết thúc sự kiện')->withInput();
            if (empty($request->get('Address')))
                return redirect()->back()->with('error', 'Vui lòng không để trống địa chỉ diễn ra sự kiện')->withInput();

            $event = new Event();
            $event->Title = StringHelper::convertUtf8($request->get('Title'));
            $event->StartDate = $request->get('StartDate') ? $this->formatDate($request->get('StartDate')) : '';
            $event->EndDate = $request->get('EndDate') ? $this->formatDate($request->get('EndDate')) : '';
            $event->Address = StringHelper::convertUtf8($request->get('Address'));
            $event->Description = StringHelper::convertUtf8($request->get('Description'));
            $result = DB::transaction(function () use ($event, $request) {
                $event->save();
                if (count($request->StaffId)) {
                    foreach ($request->StaffId as $staffId) {
                        $eventStaff = new EventStaff();
                        $eventStaff->EventId = $event->id;
                        $eventStaff->StaffId = $staffId;
                        $eventStaff->Status = 1;
                        $eventStaff->ProposeByStaffId = 0;
                        $eventStaff->save();
                        $this->sendMail($event, $eventStaff);
                    }
                }
                return redirect('/su-kien/danh-sach')->with('success', 'Thêm thành công');
            });
            return $result;
        } catch (\Exception $e) {
            dd($e);
            return redirect('/su-kien/danh-sach')->with('error', 'Thêm thất bại');
        }

    }

    public function Edit($EventId)
    {
        $event = Event::where('EventId', $EventId)->first();
        $eventStaff = EventStaff::where('EventId', $EventId)->get();
        $arr = [];
        foreach ($eventStaff as $e) {
            $arr[] = $e->StaffId;
        }
        $department = Department::orderBy('DepartmentName')->select('DepartmentId', 'DepartmentName')->get();
        foreach ($department as $key => $item) {
            $staff = Staff::leftjoin('StaffType', 'StaffType.StaffTypeId', '=', 'Staff.StaffTypeId')
                ->where('DepartmentId', $item->DepartmentId)
                ->orderByDesc('Staff.updated_at')
                ->select('Staff.StaffId', 'Staff.StaffName',
                    'StaffType.StaffTypeName')
                ->get();
            foreach ($staff as $k => $st) {
                if (in_array($st->StaffId, $arr)) {
                    $staff[$k]->selected = 1;
                } else {
                    $staff[$k]->selected = 0;
                }
            }
            $department[$key]->staff = $staff;
        }
        if ($event) {
            return view('event/edit', [
                'event' => $event,
                'department' => $department
            ]);
        } else {
            return redirect('/su-kien/danh-sach')->with('error', 'Xem thất bại');
        }
    }

    public function Update(Request $request, $EventId)
    {
        try {
            if (empty($request->get('Title')))
                return redirect()->back()->with('error', 'Vui lòng không để trống tên sự kiện')->withInput();
            if (empty($request->get('StartDate')))
                return redirect()->back()->with('error', 'Vui lòng không để trống thời gian bắt đầu sự kiện')->withInput();
            if (empty($request->get('EndDate')))
                return redirect()->back()->with('error', 'Vui lòng không để trống thời gian kết thúc sự kiện')->withInput();
            if (empty($request->get('Address')))
                return redirect()->back()->with('error', 'Vui lòng không để trống địa chỉ diễn ra sự kiện')->withInput();
            $result = DB::transaction(function () use ($request, $EventId) {
                DB::table('Event')->where('EventId', $EventId)->update([
                    'Title' => StringHelper::convertUtf8($request->get('Title')),
                    'StartDate' => $request->get('StartDate') ? $this->formatDate($request->get('StartDate')) : NULL,
                    'EndDate' => $request->get('EndDate') ? $this->formatDate($request->get('EndDate')) : NULL,
                    'Address' => StringHelper::convertUtf8($request->get('Address')),
                    'Description' => StringHelper::convertUtf8($request->get('Description')),
                ]);
                $event = EventStaff::where('EventId', $EventId)->delete();
                if (count($request->StaffId)) {
                    foreach ($request->StaffId as $staffId) {
                        $eventStaff = new EventStaff();
                        $eventStaff->EventId = $EventId;
                        $eventStaff->StaffId = $staffId;
                        $eventStaff->Status = 1;
                        $eventStaff->ProposeByStaffId = 0;
                        $eventStaff->save();
                    }
                }
                return redirect('/su-kien/danh-sach')->with('success', 'Cập nhật thành công');
            });
            return $result;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function Delete($EventId)
    {
        try {

            $result = DB::transaction(function () use ($EventId) {
                $event = Event::where('EventId', $EventId)->delete();
                $eventStaff = EventStaff::where('EventId', $EventId)->delete();
                return redirect('/su-kien/danh-sach')->with('success', 'Xóa thành công');
            });
            return $result;

        } catch (\Exception $ex) {
            return redirect('/su-kien/danh-sach')->with('error', 'Xóa thất bại');
        }
    }

    public function ListEventStaff($EventId)
    {
        try {
            $event = Event::where('EventId', $EventId)->first();
            $eventStaff = EventStaff::where('EventId', $EventId)
                ->join('Staff', 'EventStaff.StaffId', '=', 'Staff.StaffId')
                ->leftjoin('Department', 'Department.DepartmentId', '=', 'Staff.DepartmentId')
                ->orderByDesc('Staff.updated_at')
                ->select('Staff.StaffId', 'Staff.StaffName',
                    'Department.DepartmentName', 'EventStaff.Status', 'EventStaff.ProposeByStaffId')
                ->get();
            $staff0 = EventStaff::where('EventId', $EventId)->where('Status', 0)->count();
            $staff1 = EventStaff::where('EventId', $EventId)->where('Status', 1)->count();
            $staff2 = EventStaff::where('EventId', $EventId)->where('Status', 2)->count();
            $user = KeepingTime::where('EventId', $EventId)->where('Type', 2)->get();
//            dd($user);
            return view('event/listStaff', [
                'staff' => $eventStaff,
                'event' => $event,
                'staff0' => $staff0,
                'staff1' => $staff1,
                'staff2' => $staff2,
                'user' => $user
            ]);
        } catch (\Exception $e) {
            return redirect('/thong-ke/su-kien')->with('error', 'Hiển thị thất bại');
        }
    }

    public function DeleteEventStaff($EventId, $StaffId)
    {
        try {
            $event = EventStaff::where('EventId', $EventId)->where('StaffId', $StaffId)->delete();
            return redirect()->back()->with('success', 'Xóa thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Xóa thất bại');
        }
    }
}
