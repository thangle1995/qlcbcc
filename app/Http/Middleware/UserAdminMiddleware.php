<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use App\Models\UserPermission;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('admin')){
            $user=session()->get('admin');
            $route=$request->route();

            if($route->uri()=='/'){
                return $next($request);
            }else{
                $arr=explode('/',$route->uri());
                $route=$arr[0].'/'.$arr[1];
                $arr_link=[
                    'quan-tri/thong-tin',
                    'quan-tri/change-pass',
                    'staff/leave-of-absence',
                    'thong-bao/su-kien',
                    'don-nghi/danh-sach',
                    'don-nghi/tao-moi',
                    'don-nghi/cap-nhat',
                    'don-nghi/xoa',
                    'quan-tri/danh-sach',
                    'quan-tri/tao-moi',
                    'quan-tri/cap-nhat',
                    'phong-ban/don-xin-nghi',
                    'phong-ban/don-xin-nghi',
                    'quan-tri/profile-hơme',
                    'cham-cong/staff'
                ];
                $check=$this->getParentPermission1($user->StaffId,'nhan-vien');
                if($check){
                    array_push($arr_link,'thong-bao/nhan-vien');
                }
                if(in_array($route,$arr_link)){
                    return $next($request);
                }else{
//                    $parent=$this->getParentPermission($arr[0]);
//                    if($parent){
//                        $permission=UserPermission::join('Permission','UserPermission.PermissionId','=','Permission.PermissionId')
//                            ->where('UserPermission.StaffId',$user->StaffId)
//                            ->where('Permission.ParentId',$parent->PermissionId)
//                            ->where('Permission.Link',$route)->exists();
//                        if($permission){
//                            return $next($request);
//                        }else{
//                            return back()->with('middleware',0);
//                        }
//                    }else{
//                        return back()->with('middleware',0);
//                    }

                    $permission=UserPermission::join('Permission','UserPermission.PermissionId','=','Permission.PermissionId')
                        ->where('UserPermission.StaffId',$user->StaffId)
//                        ->where('Permission.ParentId',$parent->PermissionId)
                        ->where('Permission.Link',$route)->exists();
                    if($permission){
                        return $next($request);
                    }else{
                        return back()->with('middleware',0);
                    }
                }
            }
        }

        return redirect('/login');
    }
    public function getParentPermission($link){
        $permission=Permission::where('Link',$link)->first();
        return $permission;
    }
    public function getParentPermission1($userId,$link){
        $parent=Permission::where('Link',$link)->first();
        $permission=UserPermission::join('Permission','UserPermission.PermissionId','=','Permission.PermissionId')
            ->where('UserPermission.StaffId',$userId)
            ->where('Permission.ParentId',$parent->PermissionId)->first();
        return $permission;
    }
}
