<?php
/**
 * Created by PhpStorm.
 * User: BSTAR
 * Date: 5/4/2018
 * Time: 11:15 AM
 */

namespace App\Common;


class StringHelper
{
    public static function convertUtf8( $value ) {
        return mb_detect_encoding($value, mb_detect_order(), true) === 'UTF-8' ? $value : mb_convert_encoding($value, 'UTF-8');
    }
}