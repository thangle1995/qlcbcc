<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendConfirmEvent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $staff_name;
    protected $event_name;
    protected $start;
    protected $end;
    protected $address;
    protected $description;
    protected $event_staff_id;
    protected $token;


    /**
     * SendConfirmEvent constructor.
     * @param $staff_name
     * @param $event_name
     * @param $start
     * @param $end
     * @param $address
     * @param $description
     * @param $event_staff_id
     * @param $token
     */
    public function __construct($staff_name, $event_name, $start, $end, $address, $description, $event_staff_id, $token)
    {
        $this->staff_name = $staff_name;
        $this->event_name = $event_name;
        $this->start = $start;
        $this->end = $end;
        $this->address = $address;
        $this->description = $description;
        $this->event_staff_id = $event_staff_id;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->view('send_mail_event')->with([
            'event_name' => $this->event_name,
            'staff_name' => $this->staff_name,
            'start' => Carbon::parse($this->start)->format('d/m/Y H:s'),
            'end' => Carbon::parse($this->end)->format('d/m/Y H:s'),
            'address' => $this->address,
            'description' => $this->description,
            'event_staff_id' => $this->event_staff_id,
            'token' => $this->token
        ]);
    }
}
