@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Đơn xin nghỉ việc</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/don-nghi/tao-moi" class="btn btn-lg btn-success">Thêm đơn mới</a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Ngày bắt đầu</th>
                                        <th>Ngày kết thúc</th>
                                        <th>Tổng ngày nghỉ</th>
                                        <th>Lý do</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($leave)
                                        @foreach($leave as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{\Carbon\Carbon::parse($d->DateStart)->format('d/m/Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($d->DateEnd)->format('d/m/Y')}}</td>
                                                <td>{{$d->TotalDate}}</td>
                                                <td>{{$d->Reason}}</td>
                                                <td>
                                                    @if($d->Status)
                                                        <label class="label label-success">Đã duyệt</label>
                                                    @else
                                                        <label class="label label-danger">Chưa duyệt</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($d->Status)
                                                        <label class="label label-success">Đã duyệt</label>
                                                    @else
                                                        <a href="{{url('/')}}/don-nghi/cap-nhat/{{$d->LeaveOfAbsenceId}}" class="btn btn-sm btn-warning">Sửa</a>
                                                        <button onclick="deleteObj('.group-{{$d->LeaveOfAbsenceId}}','{{url('/')}}/don-nghi/xoa/{{$d->LeaveOfAbsenceId}}','Đơn xin nghỉ')" class="btn btn-sm btn-danger">Xóa</button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
