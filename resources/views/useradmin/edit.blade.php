@extends('layout.master')
<style>
    .bootstrap-select.btn-group .dropdown-menu{
        top: 70px;
    }
</style>
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Nhân viên</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Phân quyền</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/nhan-vien/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/quan-tri/cap-nhat/{{$staff->StaffId}}" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Mã nhân viên</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label>{{$staff->StaffId}}</label>
                                    </div>
                                </div>
                                <label for="email_address">Họ và tên</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <label>{{$staff->StaffName}}</label>
                                    </div>
                                </div>
                                <label for="email_address">Mật khẩu (ít nhất 8 ký tự)</label><br>
                                <label for="email_address" style="font-size: 10px;font-weight: 300;font-style: inherit;">Nếu để trống mật khẩu sẽ không đổi</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="Password" class="form-control">
                                    </div>
                                </div>

                                <label for="password">Trạng Thái</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="radio" class="with-gap" value="1" @if($staff->IsActive) checked @endif  name="IsActive" id="ig_radio">
                                            <label for="ig_radio" style="margin-right: 30px">Hoạt động</label>
                                             <input type="radio" value="0" @if(!$staff->IsActive) checked @endif class="with-gap" name="IsActive" id="ig_radio1">
                                            <label for="ig_radio1">Ngưng hoạt động</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    Danh sách tham gia
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <select name="Permission[]" id="optgroup" class="ms searchable" multiple="multiple">
                                                    @if(count($permission)!=0)
                                                        @foreach($permission as $item)
                                                            <optgroup label="{{$item->Name}}">
                                                                @if(count($item->child)!=0)
                                                                    @foreach($item->child as $p)
                                                                        <option value="{{$p->PermissionId}}" @if($p->selected) selected @endif>{{$p->Name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .search-input{
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('scripts')
<script>
    $('.searchable').multiSelect({
        selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Tìm kiếm...'>",
        selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Tìm kiếm...'>",
        afterInit: function(ms){
            var that = this;
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });
</script>
@endsection
