@extends('layout.master')
<style>
    .bootstrap-select.btn-group .dropdown-menu{
        top: 70px;
    }
</style>
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Quản trị</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Đổi mật khẩu</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/quan-tri/change-pass" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Mật khẩu hiện tại</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="Password" class="form-control"
                                               placeholder="Nhập vào mật khẩu hiện tại.">
                                    </div>
                                </div>
                                <label for="email_address">Mật khẩu mới</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="PasswordNew" class="form-control"
                                               placeholder="Nhập vào mật khẩu mới.">
                                    </div>
                                </div>
                                <label for="email_address">Xác nhận mật khẩu mới</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="PasswordNewCf" class="form-control"
                                               placeholder="Xác nhận mật khẩu mới.">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
