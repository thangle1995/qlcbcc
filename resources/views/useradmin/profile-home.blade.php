
@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Thông tin cá nhân</a></li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd-5">
                    <div class="card">
                        <div class="body pd-5" style="padding-top: 40px!important;">
                            <div class="col-md-6 pd-5">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pd-5">
                                    <div class="info-box bg-date1 hover-expand-effect">
                                        <div class="icon">
                                            <img src="{{asset('img/u1.png')}}" class="img-responsive">
                                        </div>
                                        <div class="content">
                                            <p class="text">Số ngày đi làm</p>
                                            <span>{{$date1}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pd-5">
                                    <div class="info-box bg-date2 hover-expand-effect">
                                        <div class="icon">
                                            <img src="{{asset('img/u2.png')}}" class="img-responsive">
                                        </div>
                                        <div class="content">
                                            <p class="text">Số ngày đúng giờ</p>
                                            <span>{{$date2}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pd-5">
                                    <div class="info-box bg-date3 hover-expand-effect">
                                        <div class="icon">
                                            <img src="{{asset('img/u3.png')}}" class="img-responsive">
                                        </div>
                                        <div class="content">
                                            <p class="text">Số ngày vắng</p>
                                            <span>{{$date3}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pd-5">
                                    <div class="info-box bg-date4 hover-expand-effect">
                                        <div class="icon">
                                            <img src="{{asset('img/u4.png')}}" class="img-responsive">
                                        </div>
                                        <div class="content">
                                            <p class="text">Số ngày muộn</p>
                                            <span>{{$date4}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding-top: 10px">
                                <div class="col-md-4 col-sm-6 col-xs-12 pd-0 mg-b-0">
                                    <img src="{{asset('img/user.png')}}" class="img-responsive" style="width: 100%">

                                </div>
                                <div class="col-md-8 col-sm-6 col-xs-12 pd-0 mg-b-0">
                                    <h3 class="bg-cbcc-name">{{$staff->StaffName}}</h3>
                                    <div class="p-content">
                                        <p>Số thẻ: <strong>{{$staff->MifareCardId or '--/--'}}</strong></p>
                                        <p>Phòng: <strong>{{$staff->reDepartment ? $staff->reDepartment->DepartmentName :'--/--'}}</strong></p>
                                        <p>Chức vụ: <strong>{{$staff->reTypeStaff ? $staff->reTypeStaff->StaffTypeName :'--/--'}}</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-12 mg-b-0 pd-0">
                                    <div class="col-md-6 col-sm-6 col-xs-12 bg-cbcc">
                                        <p class="p-title">Mã CBCC: <strong>{{$staff->StaffId or '--/--'}}</strong></p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 bg-deep-orange">
                                        <p class="p-title">Điểm thi đua: <strong>{{$score->Score or '0'}} - Tổng: {{$score_total or ''}}</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-12 mg-b-0 text-right pull-right">
                                    <a href="{{url('quan-tri/thong-tin')}}">Cập nhật thông tin cá nhân</a>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .pd-5{
            padding: 5px !important;
        }
        .pd-0{
            padding: 0px !important;
        }
        .p-content{
            padding: 10px;
        }
        .info-box .content{
            padding: 7px 5px;
            text-align: center;
        }
        .info-box .content span{
            font-size: 35px;
            font-weight: 700;
        }
        .info-box .content .text{
            color: #fff !important;
            margin-top: 2px;
            margin-bottom: 0px;
        }
        .bg-date1{
            background-color: rgb(94, 194, 210) !important;
            color: #fff !important;
        }
        .bg-date2{
            background-color: rgb(255, 107, 107) !important;
            color: #fff !important;
        }
        .bg-date3{
            background-color: rgb(115, 102, 190) !important;
            color: #fff !important;
        }
        .bg-date4{
            background-color: rgb(132, 199, 106) !important;
            color: #fff !important;
        }
        .bg-cbcc{
            background-color: rgb(0, 52, 97) !important;
            color: #fff !important;
        }
        .bg-cbcc-name{
            background-color: rgb(13, 181, 123) !important;
            color: #fff !important;
            padding: 5px;
            margin-top: 0px;
            margin-bottom: 5px;
        }
        .info-box .icon img {
            width: 80px;
            height: 80px;
        }
        .info-box .icon i {
            font-size: 30px;
        }
        .p-title{
            padding: 5px 0px;
            margin-bottom: 0px;
        }
        .info-box{
            margin-bottom: 0px;
        }
        .mg-b-0{
            margin-bottom: 0px!important;
        }
    </style>
@endsection

