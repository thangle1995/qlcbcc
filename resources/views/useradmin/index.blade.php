@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Quản trị viên</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                            {{--<a href="{{url('/')}}/quan-tri/tao-moi" class="btn btn-lg btn-success">Thêm quản trị mới</a>--}}
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Mã nhân viên</th>
                                        <th>Họ tên</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($user)
                                        @foreach($user as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->StaffName}}</td>
                                                <td>
                                                    @if($d->IsActive)
                                                        <label class="label label-success">Hoạt động</label>
                                                    @else
                                                        <label class="label label-danger">Ngưng hoạt động</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{url('/')}}/quan-tri/cap-nhat/{{$d->StaffId}}" class="btn btn-sm btn-warning">Cập nhật</a>
                                                    @if(session('admin')->StaffId!=$d->StaffId)
                                                        <button onclick="deleteObj('.group-{{$d->StaffId}}','{{url('/')}}/quan-tri/xoa/{{$d->StaffId}}','{{$d->StaffName}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                        {{--<th>Số thứ tự</th>--}}
                                        {{--<th>Email</th>--}}
                                        {{--<th>Trạng thái</th>--}}
                                        {{--<th></th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
