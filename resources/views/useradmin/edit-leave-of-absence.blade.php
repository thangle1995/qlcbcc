@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Đơn xin nghỉ việc</a></li>
                    <li class="active"><i class="material-icons">list</i> Cập nhật</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                @include('partial.alert')
                                <a href="{{url('/')}}/don-nghi/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                            </div>
                            <div class="body">
                                <form action="{{url('/')}}/don-nghi/cap-nhat/{{$leave->LeaveOfAbsenceId}}" method="post">
                                    {{ csrf_field() }}
                                    <label for="email_address">Từ ngày</label>
                                    <div class="input-group demo-masked-input">
                                        <div class="form-line">
                                            <input type="text" name="DateStart" class="form-control date" value="{{\Carbon\Carbon::parse($leave->DateStart)->format('d/m/Y')}}"
                                                   placeholder="Nhập ngày bắt đầu nghỉ">
                                        </div>
                                    </div>
                                    <label for="email_address">Đến ngày</label>
                                    <div class="input-group demo-masked-input">
                                        <div class="form-line">
                                            <input type="text" name="DateEnd" class="form-control date" value="{{\Carbon\Carbon::parse($leave->DateEnd)->format('d/m/Y')}}"
                                                   placeholder="Nhập ngày kết thúc">
                                        </div>
                                    </div>
                                    <label for="email_address">Tổng số ngày nghỉ</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="TotalDate" class="form-control" value="{{$leave->TotalDate}}"
                                                   placeholder="1 ngày">
                                        </div>
                                    </div>
                                    <label for="email_address">Lý do</label>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <textarea name="Reason" class="form-control" placeholder="Nhập lý do...">{{$leave->Reason}}</textarea>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                    <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                                </form>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </section>
@endsection
