@extends('layout.master')
<style>
    .bootstrap-select.btn-group .dropdown-menu{
        top: 70px;
    }
</style>
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Quản trị</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Thêm mới</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/quan-tri/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/quan-tri/tao-moi" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Email (dùng để đăng nhập)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="Email" class="form-control" value="{{old("Email")}}"
                                               placeholder="Nhập vào email. Ví dụ: nguyen@gmail.com">
                                    </div>
                                </div>
                                <label for="email_address">Mật khẩu (ít nhất 8 ký tự)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="Password" class="form-control" value="{{old("Password")}}">
                                    </div>
                                </div>
                                <label for="email_address">Chọn nhân viên</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="StaffId" data-live-search="true">
                                            <option value="0">--Nhân viên--</option>
                                            @if(count($staff)!=0)
                                                @foreach($staff as $item)
                                                    <option value="{{$item->StaffId}}">{{$item->StaffId}} - {{$item->StaffName}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <label for="password">Trạng Thái</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="radio" class="with-gap" value="1" checked name="IsActive" id="ig_radio">
                                            <label for="ig_radio" style="margin-right: 30px">Hoạt động</label>
                                             <input type="radio" value="0" class="with-gap" name="IsActive" id="ig_radio1">
                                            <label for="ig_radio1">Ngưng hoạt động</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    Quyền sử dụng
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <select name="Permission[]" id="optgroup" data-live-search="true" class="ms" multiple="multiple">
                                                    @if(count($permission)!=0)
                                                        @foreach($permission as $item)
                                                            <optgroup label="{{$item->Name}}">
                                                                @if(count($item->child)!=0)
                                                                    @foreach($item->child as $p)
                                                                        <option value="{{$p->PermissionId}}">{{$p->Name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Thêm người quản trị</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
