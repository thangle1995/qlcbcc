@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Cấu hình</a></li>
                    <li class="active"><i class="material-icons">list</i> Ngày nghỉ</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        @include('partial.alert')
                        <form action="{{url('/ngay-nghi/trong-tuan')}}" method="post">
                            {{ csrf_field() }}
                            <div class="body col-md-offset-4">
                                @foreach($dayOff as $item)
                                    <div class="input-group input-group-lg" style="margin-bottom: 0px">
                                    <span class="input-group-addon" style="text-align: left">
                                        <input type="checkbox" class="with-gap" @if($item->Status) checked @endif value="{{$item->DayOffInWeekId}}" name="{{$item->DayOffInWeekId}}" id="lb-{{$item->DayOffInWeekId}}">
                                        <label for="lb-{{$item->DayOffInWeekId}}" style="margin-right: 30px">{{$item->Name}}</label>
                                    </span>
                                    </div>
                                @endforeach
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
