@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Cấu hình</a></li>
                    <li class="active"><i class="material-icons">list</i> Ngày nghỉ</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                            <a onclick="ftCreateDay()" class="btn btn-lg btn-success">Thêm ngày nghỉ</a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Ngày nghỉ</th>
                                        <th>Ghi chú</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($holiday)
                                        @foreach($holiday as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{Carbon\Carbon::parse($d->Date)->format('d/m/Y')}}</td>
                                                <td>{{$d->Note}}</td>
                                                <td>
                                                    <a onclick="ftUpdate('{{$d->HolidayId}}','{{Carbon\Carbon::parse($d->Date)->format('d/m/Y')}}','{{$d->Note}}')" class="btn btn-sm btn-warning">Cập nhật</a>
                                                    <button onclick="deleteObj('.group-{{$d->HolidayId}}','{{url('/')}}/ngay-nghi/ngay-le/xoa/{{$d->HolidayId}}','{{Carbon\Carbon::parse($d->Date)->format('d/m/Y')}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Ngày nghỉ</th>
                                        <th>Ghi chú</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                        <div id="modalDay" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Thông tin ngày nghỉ lễ</h4>
                                    </div>
                                    <form action="{{url('ngay-nghi/ngay-le/cap-nhat')}}" method="post">
                                    <div class="modal-body">
                                        {{ csrf_field() }}
                                        <input type="text" id="Id" name="Id" hidden>
                                        <label for="">Ngày nghỉ</label>
                                        <div class="input-group demo-masked-input">
                                            <div class="form-line">
                                                <input type="text" id="Date" name="Date" class="form-control date"
                                                       placeholder="Nhập vào ngày nghỉ lễ. Ví dụ: 30/04/2016">
                                            </div>
                                        </div>
                                        <label for="email_address">Ghi chú</label>
                                        <div class="input-group">
                                            <div class="form-line">
                                                <textarea name="Note" id="Note" class="form-control" placeholder="Nhập ghi chú cho ngày nghỉ..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success">Lưu</button>
                                    </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script>
    function ftCreateDay() {
        $('#Id').val('');
        $('#Date').val('');
        $('#Note').val('');
        $('#modalDay').modal('show');
    }
    function ftUpdate(Id, Date, Note) {
        $('#Id').val(Id);
        $('#Date').val(Date);
        $('#Note').val(Note);
        $('#modalDay').modal('show');
    }
</script>    
@endsection
