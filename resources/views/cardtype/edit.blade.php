@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Loại thẻ</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Cập nhật</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/loai-the/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/loai-the/cap-nhat/{{$cardtype->MifareCardTypeId}}" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Tên loại thẻ</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="MifareCardTypeName" class="form-control"
                                               placeholder="Nhập vào tên loại thẻ. Ví dụ: CBCC" value="{{$cardtype->MifareCardTypeName}}">
                                    </div>
                                </div>
                                <label for="password">Trạng thái</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="radio" class="with-gap" value="1" name="IsActive" @if($cardtype->IsActive) checked @endif id="ig_radio">
                                            <label for="ig_radio" style="margin-right: 30px">Hoạt động</label>
                                             <input type="radio" class="with-gap" value="0" name="IsActive" @if(!$cardtype->IsActive) checked @endif id="ig_radio1">
                                            <label for="ig_radio1">Ngưng hoạt động</label>
                                        </span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
