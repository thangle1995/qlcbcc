@extends('layout.master')

    <style>
        .lb-late{
            top: 10px;
            position: absolute;
            right: 10px;
            border-radius: 5px!important;
            padding-top: 5px!important;
        }
    </style>


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ </a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i>Chấm công > {{$staff->reDepartment->DepartmentName}}</a></li>
                    <li class="active"><i class="material-icons">list</i> {{$staff->StaffName}} - {{$staff->StaffId}} ({{$staff->reTypeStaff->StaffTypeName}})</li>
                </ol>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/thong-ke/cham-cong/{{$staff->reDepartment->DepartmentId}}" class="btn btn-lg btn-danger">Trở về</a>
                            <a href="{{url('/')}}/thong-ke/cham-cong/staff/{{$staff->StaffId}}" class="btn btn-lg btn-success">Refresh</a>
                        </div>
                        <div class="body clearfix">
                            <div class="col-md-12">
                                <form action="{{url('/')}}/thong-ke/cham-cong/staff/{{$staff->StaffId}}" method="post">

                                    <input hidden name="_token" value="{{csrf_token()}}">
                                    <div class="col-md-4">
                                        <label for="email_address">Chọn năm</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <div class="form-line">
                                                    <select class="form-control" name="year">
                                                        @for($i=0; $i<= 10; $i++)
                                                            <option value="{{Carbon\Carbon::now()->year - $i}}" @if(Carbon\Carbon::now()->year - $i==$year) selected @endif>{{Carbon\Carbon::now()->year -$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="email_address">Chọn Tháng</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <div class="form-line">
                                                    <select class="form-control" name="month">
                                                        @for($j=1; $j<= 12; $j++)
                                                            <option value="{{$j}}" @if($j==$month) selected @endif>{{$j}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <button type="submit" class="btn btn-info">Tìm kiếm</button>
                                </div>
                                </form>
                            </div>
                            <div class="col-md-12 row">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 14%">Chủ nhật</th>
                                            <th  style="width: 14%">Thứ 2</th>
                                            <th style="width: 14%">Thứ 3</th>
                                            <th style="width: 14%">Thứ 4</th>
                                            <th style="width: 14%">Thứ 5</th>
                                            <th style="width: 14%">Thứ 6</th>
                                            <th style="width: 14%">Thứ 7</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--@if($department->staff())--}}
                                        @foreach($week as $w)
                                            <tr>
                                                @foreach($date as $d)
                                                    @if(isset($w[$d]))
                                                        <td style="position: relative" class="text-left">
                                                            <label>{{\Carbon\Carbon::parse($w[$d])->format('d')}}</label>
                                                            @foreach($keepingTime as $keeping)
                                                                @if(\Carbon\Carbon::parse($w[$d])->format('d-m-y')==\Carbon\Carbon::parse($keeping->KeepingTime)->format('d-m-y'))
                                                                    <label class="label label-success">Giờ vào làm: {{\Carbon\Carbon::parse($keeping->KeepingTime)->format('H:i:s')}}</label><br>
                                                                    <label class="clearfix">---</label>
                                                                    <label class="label label-warning">Giờ ra về: {{\Carbon\Carbon::parse($keeping->CheckOutTime)->format('H:i:s')}}</label>
                                                                    @if($keeping->isLate)
                                                                        <label class="label label-danger lb-late" title="Lý do: {{$keeping->Note ? $keeping->Note:'Không có lý do'}}">
                                                                            <a style="color: #fff; cursor: pointer" onclick="ftUpdate('{{$keeping->KeepingTimeId}}','{{$keeping->Note}}')">

                                                                                Trễ
                                                                                <i style="font-size: 12px" class="material-icons">create</i>
                                                                            </a>
                                                                        </label>
                                                                    @endif

                                                                    @break
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    @else
                                                        <td></td>
                                                    @endif

                                                @endforeach
                                            </tr>
                                        @endforeach

                                        </tbody>

                                    </table>
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <form action="{{url('/thong-ke')}}/cham-cong/cap-nhat/ly-do" method="post">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Cập nhật lý do</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                        <input class="form-control" id="KeepingTimeId" name="KeepingTimeId" style="display: none">
                                                        <input class="form-control" id="StaffId" name="StaffId" style="display: none" value="{{$staff->StaffId}}">
                                                        <label for="email_address">Lý do đi trễ</label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <div class="form-line">
                                                                    <input type="text" name="Note" class="form-control" id="Note"
                                                                           placeholder="Nhập lý do đi trễ...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Cập nhật</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script>
        function ftUpdate(id,note) {
            $('#KeepingTimeId').val(id);
            $('#Note').val(note);
            $('#myModal').modal('show');
        }
    </script>
@endsection
