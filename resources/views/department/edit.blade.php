@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Phòng ban</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Cập nhật</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/phong-ban/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/phong-ban/cap-nhat/{{$department->DepartmentId}}" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Mã Phòng</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="BaseDepartmentId" class="form-control"
                                               placeholder="Nhập vào mã phòng. Ví dụ: DM_BP171200000005" value="{{$department->BaseDepartmentId}}">
                                    </div>
                                </div>
                                <label for="password">Tên phòng ban</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="DepartmentName" class="form-control"
                                               placeholder="Nhập vào tên phòng. Ví dụ: Trung tâm giáo dục thường xuyên quận 12" value="{{$department->DepartmentName}}">
                                    </div>
                                </div>
                                <label for="password">Thời gian bắt đầu làm việc</label>
                                <div class="form-group">
                                    <div class="input-group demo-masked-input">
                                        <div class="form-line">
                                            <input type="text" name="StartTime" class="form-control time24"  value="{{$department->StartTime}}"
                                                   placeholder="Nhập thời gian làm việc. Ví dụ: 08:00">
                                        </div>
                                    </div>
                                </div>
                                <label for="password">Thời gian trì hoãn (phút)</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="number" name="DelayTime" class="form-control"  value="{{$department->DelayTime}}"
                                                   placeholder="Nhập thời gian trì hoãn. Ví dụ: 15">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
