@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Phòng ban</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách </li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Tên phòng ban</th>
                                        <th>Mã phòng ban</th>
                                        <th>Số lượng nhân viên</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($department)
                                        @foreach($department as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->DepartmentName}}</td>
                                                <td>{{$d->BaseDepartmentId}}</td>
                                                <td>
                                                    <a href="{{url('/')}}/thong-ke/cham-cong/{{$d->DepartmentId}}" class="btn btn-sm btn-warning">{{$d->count_staff()}}</a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Tên phòng ban</th>
                                        <th>Mã phòng ban</th>
                                        <th>Số lượng nhân viên</th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
