@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ </a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i>Chấm công > {{$department->DepartmentName}}</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách nhân viên</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body clearfix">
                            <div class="header">
                                @include('partial.alert')
                                <a href="{{url('/')}}/thong-ke/cham-cong" class="btn btn-lg btn-danger">Trở về</a>
                                <a href="{{url('/')}}/thong-ke/cham-cong/{{$department->DepartmentId}}" class="btn btn-lg btn-success">Refresh</a>
                                <div class="col-md-8 pull-right clearfix" style="margin-bottom: 10px">
                                    <form action="{{url('/')}}/thong-ke/cham-cong/{{$department->DepartmentId}}" method="post">
                                        <input hidden name="_token" value="{{csrf_token()}}">
                                        <div class="col-md-4">
                                            <label for="email_address">Chọn năm</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <div class="form-line">
                                                        <select class="form-control" name="year">
                                                            @for($i=0; $i<= 10; $i++)
                                                                <option value="{{Carbon\Carbon::now()->year -$i}}" @if(Carbon\Carbon::now()->year -$i ==$year) selected @endif>{{Carbon\Carbon::now()->year -$i}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email_address">Chọn Tháng</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <div class="form-line">
                                                        <select class="form-control" name="month">
                                                            @for($j=1; $j<= 12; $j++)
                                                                <option value="{{$j}}" @if($j==$month) selected @endif>{{$j}}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-info">Tìm kiếm</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-md-12 row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Số ngày đi làm</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($staff))
                                        @foreach($staff as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->StaffName}}</td>
                                                <td>{{$d->MifareCardId}}</td>
                                                <td>{{$d->reTypeStaff ? $d->reTypeStaff->StaffTypeName :''}}</td>
                                                <td>{{$d->worktime}}</td>
                                                <td>
                                                    <a href="{{url('/')}}/thong-ke/cham-cong/staff/{{$d->StaffId}}" class="btn btn-sm btn-success">Xem chi tiết</a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Số ngày đi làm</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
