<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Quản lý cán bộ công chức quận 12</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    {{--<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">--}}
    <link href="{{url('/')}}/css/icon.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{url('/')}}/css/bootstrap.css" rel="stylesheet">
    <link href="{{url('/')}}/css/waves.css" rel="stylesheet" />
<!-- Animation Css -->
    <link href="{{url('/')}}/css/animate.min.css" rel="stylesheet" />
    <link href="{{url('/')}}/css/multi-select.css" rel="stylesheet">

    <link href="{{url('/')}}/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="{{url('/')}}/css/dataTables.bootstrap.min.css" rel="stylesheet">
<!-- Custom Css -->

    <link href="{{url('/')}}/css/jquery-confirm.min.css" rel="stylesheet">

    <link href="{{url('/')}}/css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    {{--<link href="{{url('/')}}/css/theme-blue.css" rel="stylesheet" />--}}
    <link href="{{url('/')}}/css/theme-green.css" rel="stylesheet" />
    <script src="{{url('/')}}/js/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{url('/')}}/js/bootstrap.min.js"></script>
</head>

<body class="theme-green">
<!-- Page Loader -->
{{--<div class="page-loader-wrapper">--}}
{{--<div class="loader">--}}
{{--<div class="preloader">--}}
{{--<div class="spinner-layer pl-red">--}}
{{--<div class="circle-clipper left">--}}
{{--<div class="circle"></div>--}}
{{--</div>--}}
{{--<div class="circle-clipper right">--}}
{{--<div class="circle"></div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<p>Please wait...</p>--}}
{{--</div>--}}
{{--</div>--}}
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="TÌM KIẾM...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>

@include('partial.navbar')
<!-- #Top Bar -->
@include('partial.sidebar')

@hasSection('content')
    @yield('content')
@else
    Có lỗi trong quá trình đọc nội dung...
@endif
<div id="modalMessage" class="modal modal-ms fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #9acc2a; color: #fff">
                <h4 class="modal-title">{{trans('frontend.thongbao')}}</h4>
            </div>
            <div class="modal-body">
                <p id="txtMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-spa" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>
{{--@if(session()->has('middleware'))--}}
    {{--<div class="alert alert-success">--}}
        {{--<h1>Có session nè</h1>--}}
    {{--</div>--}}
{{--@endif--}}
@if(session()->has('middleware'))
    <h1>Có session nè</h1>
    <script>
        $(function() {
            $.confirm({
                icon: 'fa fa-warning',
                title: 'Thông báo',
                content: 'Bạn không có quyền truy cập!!!',
                type: 'red',
                theme: 'light',
                typeAnimated: true,
                buttons: {
                    close: {
                        text: 'Đóng',
                        btnClass:'btn-red'
                    }
                }
            });
        });
    </script>
@endif
<!-- Jquery Core Js -->


<script src="{{url('/')}}/js/bootstrap-select.min.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{url('/')}}/js/jquery.slimscroll.js"></script>
<!-- Custom Js -->
<script src="{{url('/')}}/js/jquery-confirm.min.js"></script>


<script src="{{url('/')}}/js/delete.js"></script>
<script src="{{url('/')}}/js/jquery.multi-select.js"></script>
<script src="{{url('/')}}/js/jquery.inputmask.bundle.js"></script>
<script src="{{url('/')}}/js/advanced-form-elements.js"></script>
<script src="{{url('/')}}/js/jquery.dataTables.js"></script>
<script src="{{url('/')}}/js/dataTables.bootstrap.min.js"></script>
<script src="{{url('/')}}/js/jquery-datatable.js"></script>
<script src="{{url('/')}}/js/jquery.slimscroll.js"></script>
<script src="{{url('/')}}/js/waves.js"></script>
<script src="{{url('/')}}/js/dropzone.js"></script>
<script src="{{url('/')}}/js/admin.js"></script>
<script src="{{url('/')}}/js/demo.js"></script>
<script src="{{url('/')}}/js/jquery.quicksearch.js"></script>



@yield('scripts')
</body>

</html>