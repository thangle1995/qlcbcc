<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image" style="float: left">
                <img src="{{url('/')}}/img/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container" style="top: 10px;">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{session('admin')->StaffName}}</div>
                <div class="email">ADMIN</div>
                <div class="btn-group user-helper-dropdown" style="bottom: 0px;">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="{{url('/quan-tri/thong-tin')}}"><i class="material-icons">person</i>Thông tin cá nhân</a></li>
                        {{--<li role="seperator" class="divider"></li>--}}
                        <li><a href="{{url('/staff/leave-of-absence')}}"><i class="material-icons">group</i>Xin phép nghỉ</a></li>
                        {{--<li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>--}}
                        {{--<li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>--}}
                        <li role="seperator" class="divider"></li>
                        <li><a href="{{url('/quan-tri/change-pass')}}"><i class="material-icons">lock</i>Đổi mật khẩu</a></li>
                        <li><a href="{{url('/logout')}}"><i class="material-icons">input</i>Đăng xuất</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">CHỨC NĂNG</li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>Cá nhân</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{url('/thong-bao')}}/su-kien">Danh sách sự kiện tham gia</a>
                        </li>
                        <li>
                            <a href="{{url('/cham-cong')}}/staff">Quản lý chấm công</a>
                        </li>
                        <li>
                            <a href="{{url('/don-nghi')}}/danh-sach">Đơn xin nghỉ</a>
                        </li>
                    </ul>
                </li>
                @if(\App\Http\Controllers\DepartmentController::checkDerpartment())
                    <li>
                        <a href="{{url('/phong-ban')}}/don-xin-nghi">
                            <i class="material-icons">content_paste</i>
                            <span>Quản lý đơn xin nghỉ</span>
                        </a>
                    </li>
                @endif

                {!! \App\Http\Controllers\UserAdminController::getPermission() !!}
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2018 <a href="javascript:void(0);">Ủy ban nhân dân Quận 12</a>.
            </div>
            <div class="version">
                <b>Phiên bản: </b> 1.0.2
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <!-- #END# Right Sidebar -->
</section>