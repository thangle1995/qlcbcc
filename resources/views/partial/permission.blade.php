@if(count($permission)!=0)
    @foreach($permission as $item)
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">{{$item->icon}}</i>
                <span>{{$item->name}}</span>
            </a>
            @if(count($item->permission))
                <ul class="ml-menu">
                    @foreach($item->permission as $p)
                        @if($item->link == '#')
                            @if(strpos($p->Link,'danh-sach') || strpos($p->Link,'trong-tuan')|| strpos($p->Link,'ngay-le'))
                                <li>
                                    <a href="{{url('/')}}/{{$p->Link}}">{{$p->Name}}</a>
                                </li>
                            @endif
                        @else
                            @if(strpos($p->Link,'danh-sach') || strpos($p->Link,'tao-moi')||strpos($p->Link,'cham-cong') || strpos($p->Link,'su-kien'))
                                <li>
                                    <a href="{{url('/')}}/{{$p->Link}}">{{$p->Name}}</a>
                                </li>
                            @endif
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
@endif