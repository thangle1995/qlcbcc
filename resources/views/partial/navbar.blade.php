<nav class="navbar" style="background: #006400">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <span> <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('/')}}img/logo.png" width="50px" alt="Logo Quan 12"/></a> <h4>PHẦN MỀN QUẢN LÝ CÁN BỘ CÔNG CHỨC QUẬN 12</h4></span>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <!-- #END# Call Search -->
                <!-- Notifications -->

                {!! \App\Http\Controllers\DepartmentController::ListStaff() !!}
                {!! \App\Http\Controllers\DepartmentController::ListEvent() !!}
                <!-- #END# Notifications -->
                <!-- Tasks -->
                <!-- #END# Tasks -->
            </ul>
        </div>
    </div>
</nav>