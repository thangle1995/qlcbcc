
<li class="dropdown">
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
        <i class="material-icons">flag</i>
        <span class="label-count">{{count($event)}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">SỰ KIỆN</li>
        <li class="body">

            <ul class="menu">
                @if(count($event)!=0)
                    @foreach($event as $e)
                        <li style="list-style: none;">
                            <a href="{{url('/')}}/thong-bao/su-kien/{{$e->EventId}}" class=" waves-effect waves-block">
                                <div class="icon-circle bg-light-blue">
                                    <i class="material-icons">event</i>
                                </div>
                                <div class="menu-info">
                                    <h4>{{$e->Title}}</h4>
                                    <p>
                                        <i class="material-icons">access_time</i> {{\Carbon\Carbon::parse($e->updated_at)->format('H:i d/m/Y')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </li>
        <li class="footer">
            <a href="{{url('thong-bao/su-kien')}}">Xem tất cả sự kiện</a>
        </li>
    </ul>
</li>
