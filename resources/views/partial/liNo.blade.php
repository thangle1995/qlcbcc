
<li class="dropdown">
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
        <i class="material-icons">notifications</i>
        <span class="label-count">{{$count}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">THÔNG BÁO</li>
        <li class="body">
            <ul class="menu">
                @if(count($staff)!=0)
                    @foreach($staff as $e)
                        <li style="list-style: none;">
                            <a href="{{url('/')}}/thong-bao/nhan-vien/{{$e->StaffId}}" class=" waves-effect waves-block">
                                <div class="icon-circle bg-light-green">
                                    <i class="material-icons">person</i>
                                </div>
                                <div class="menu-info">
                                    <h4>{{$e->StaffName}} - {{$e->StaffId}}</h4>
                                    <p>
                                        <i class="material-icons">access_time</i> {{\Carbon\Carbon::parse($e->updated_at)->format('H:i d/m/Y')}}
                                    </p>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </li>
        <li class="footer">
            <a href="{{url('thong-bao/nhan-vien')}}">Xem tất cả thông báo</a>
        </li>
    </ul>
</li>