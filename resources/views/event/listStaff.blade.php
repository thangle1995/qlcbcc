@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Sự kiện > {{$event->Title}}</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách nhân viên ({{count($staff)}})</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/thong-ke/su-kien" class="btn btn-lg btn-danger">Trở về</a>

                        </div>
                        <div class="body">

                            <p>Số người xác nhận tham gia: <label class="label label-success">{{$staff2}}</label></p>
                            <p>Số người đang đợi: <label class="label label-info">{{$staff1}}</label></p>
                            <p>Số người đã hủy: <label class="label label-danger">{{$staff0}}</label></p>
                            <div class="table-responsive">
                                <h3>Danh sách mời tham gia</h3>
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Chức vụ</th>
                                        <th>Phòng</th>
                                        <th>Trạng thái</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($staff)
                                        @foreach($staff as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->StaffName}}</td>
                                                <td>{{$d->StaffTypeName}}</td>
                                                <td>{{$d->DepartmentName}}</td>
                                                <td>
                                                    @if($d->Status==0)
                                                        <label class="label label-danger">Đã đề xuất người khác</label>
                                                    @endif
                                                    @if($d->Status==1)
                                                        <label class="label label-primary">Đang đợi xác nhận</label>
                                                    @endif
                                                    @if($d->Status==2)
                                                        <label class="label label-success">Đã xác nhận</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($d->ProposeByStaffId ==0)
                                                        <label class="label label-primary">Được mời</label>
                                                    @else
                                                        <label class="label label-warning">Được đề xuất bởi <br>
                                                            {!! \App\Http\Controllers\StaffController::getStaff($d->ProposeByStaffId)->StaffId !!}
                                                        </label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button onclick="deleteObj('.group-{{$d->StaffId}}','{{url('/')}}/thong-ke/su-kien/xoa/{{$event->EventId}}/{{$d->StaffId}}','{{$d->StaffName}} - {{$d->StaffId}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Chức vụ</th>
                                        <th>Phòng</th>
                                        <th>Trạng thái</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>

                            <div class="table-responsive">
                                <h3>Danh sách tham gia</h3>
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Chức vụ</th>
                                        <th>Phòng</th>
                                        <th>Thời gian đến</th>
                                        <th>Thời gian về</th>
                                        <th>Trạng thái</th>
                                        <th>Lý do</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($user)
                                        @foreach($user as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->reStaff->StaffName}}</td>
                                                <td>{{$d->reStaff->reTypeStaff->TypeStaffName}}</td>
                                                <td>{{$d->reStaff->reDepartment->DepartmentName}}</td>

                                                <td>{{\Carbon\Carbon::parse($d->KeepingTime)->format('H:i d/m/Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($d->CheckOutTime)->format('H:i d/m/Y')}}</td>
                                                <td>
                                                    @if($d->isLate)
                                                        <label class="label label-danger">Đi trễ</label>
                                                    @else
                                                        --/--
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$d->Note ? $d->Note : '--/--'}}
                                                </td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Chức vụ</th>
                                        <th>Phòng</th>
                                        <th>Thời gian đến</th>
                                        <th>Thời gian về</th>
                                        <th>Trạng thái</th>
                                        <th>Lý do</th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
