@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Sự kiện</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Số thứ tự</th>
                                            <th>Tên sự kiện</th>
                                            <th>Thời gian bắt đầu</th>
                                            <th>Thời gian kết thúc</th>
                                            <th>Số lượng tham gia</th>
                                            {{--<th></th>--}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if($event)
                                        @foreach($event as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->Title}}</td>
                                                <td>{{\Carbon\Carbon::parse($d->StartDate)->format('H:i d/m/Y')}}</td>
                                                <td>{{\Carbon\Carbon::parse($d->EndDate)->format('H:i d/m/Y')}}</td>
                                                <td><a href="{{url('/thong-ke/su-kien')}}/{{$d->EventId}}" class="btn btn-success">{{$d->count}}</a></td>
                                                {{--<td>--}}
                                                    {{--<a href="{{url('/')}}/su-kien/cap-nhat/{{$d->EventId}}" class="btn btn-sm btn-warning">Cập nhật</a>--}}
                                                    {{--<button onclick="deleteObj('.group-{{$d->EventId}}','{{url('/')}}/su-kien/xoa/{{$d->EventId}}','{{$d->Title}}')" class="btn btn-sm btn-danger">Xóa</button>--}}
                                                {{--</td>--}}
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Tên sự kiện</th>
                                        <th>Thời gian bắt đầu</th>
                                        <th>Thời gian kết thúc</th>
                                        <th>Số lượng tham gia</th>
                                        {{--<th></th>--}}
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
