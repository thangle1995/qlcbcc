@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Sự kiện</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Thêm mới</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/su-kien/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/su-kien/tao-moi" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Tên Sự kiện</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="Title" class="form-control" value="{{old("Title")}}"
                                               placeholder="Nhập vào tên sự kiện. Ví dụ: Họp cán bộ quận 12">
                                    </div>
                                </div>
                                <b>Thời gian bắt đầu sự kiện</b>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" class="form-control datetime" name="StartDate" value="{{old("StartDate")}}"
                                               placeholder="Nhập vào ngày bắt đầu sự kiện: 30/07/2016 09:30">
                                    </div>
                                </div>
                                <b>Thời gian kết thúc sự kiện</b>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" class="form-control datetime" name="EndDate" value="{{old("EndDate")}}"
                                               placeholder="Nhập vào ngày kết thúc sự kiện: 30/07/2016 11:30">
                                    </div>
                                </div>
                                <label for="email_address">Địa điểm</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="Address" class="form-control" value="{{old("Address")}}"
                                               placeholder="Nhập vào địa điểm diễn ra sự kiện. Ví dụ: Hội trường A">
                                    </div>
                                </div>
                                <label for="email_address">Nội dung sự kiện</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea name="Description" class="form-control" placeholder="Nhập vào nội dung của sự kiện...">{{old('Description')}}</textarea>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    Danh sách tham gia
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <select name="StaffId[]" id="optgroup" data-live-search="true" class="ms searchable" multiple="multiple">
                                                    @if(count($department)!=0)
                                                        @foreach($department as $item)
                                                            <optgroup label="{{$item->DepartmentName}}">
                                                                @if(count($item->staff)!=0)
                                                                    @foreach($item->staff as $staff)
                                                                        <option value="{{$staff->StaffId}}">{{$staff->StaffId }} - {{$staff->StaffName}} ({{$staff->StaffTypeName}})</option>
                                                                    @endforeach
                                                                @endif
                                                            </optgroup>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Thêm sự kiện</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .search-input{
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Tìm kiếm...'>",
            selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='Tìm kiếm...'>",
            afterInit: function(ms){
                var that = this;
                $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
    </script>
@endsection
