@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Nhân viên</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Cập nhật</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}thong-bao/nhan-vien" class="btn btn-lg btn-primary">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/nhan-vien/cap-nhat/{{$staff->StaffId}}" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Mã nhân viên</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="StaffId" class="form-control" value="{{$staff->StaffId}}"
                                               placeholder="Nhập vào mã nhân viên. Ví dụ: 20500004913">
                                    </div>
                                </div>
                                <label for="email_address">Tên nhân viên</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="StaffName" class="form-control" value="{{$staff->StaffName}}"
                                               placeholder="Nhập vào tên nhân viên. Ví dụ: Nguyễn Minh Anh">
                                    </div>
                                </div>
                                <label for="email_address">Mã thẻ</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="MifareCardId" data-live-search="true">
                                            <option value="0">--Chọn mã thẻ--</option>
                                            @if(count($card)!=0)
                                                @foreach($card as $item)
                                                    <option value="{{$item->MifareCardId}}" @if($item->MifareCardId==$staff->MifareCardId) selected @endif>{{$item->MifareCardTypeName}} ({{$item->MifareCardId}} - {{$item->Serial}})</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <label for="email_address">Ngày sinh</label>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" name="Birthday" class="form-control date" value="{{\Carbon\Carbon::parse($staff->Birthday)->format('d/m/Y')}}"
                                               placeholder="Nhập vào ngày sinh. Ví dụ: 30/07/2016">
                                    </div>
                                </div>
                                <label for="password">Giới tính</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="radio" class="with-gap" value="1" @if($staff->Gender==1) checked @endif  name="Gender" id="ig_radio">
                                            <label for="ig_radio" style="margin-right: 30px">Nam</label>
                                             <input type="radio" value="2" class="with-gap" @if($staff->Gender==2) checked @endif name="Gender" id="ig_radio1">
                                            <label for="ig_radio1">Nữ</label>
                                        </span>
                                    </div>
                                </div>
                                <label for="email_address">Chứng minh thư (CMND)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="IdentityCard" class="form-control" value="{{$staff->IdentityCard}}"
                                               placeholder="Nhập vào chứng minh thư. Ví dụ: 023551286">
                                    </div>
                                </div>
                                <label for="email_address">Ngày cấp CMND</label>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" name="DateOfIssue" class="form-control date" value="{{$staff->DateOfIssue ? \Carbon\Carbon::parse($staff->DateOfIssue)->format('d/m/Y'): ""}}"
                                               placeholder="Nhập vào ngày cấp CMND. Ví dụ: 30/07/2016">
                                    </div>
                                </div>
                                <label for="email_address">Nơi cấp CMND</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="PlaceOfIssue" class="form-control" value="{{$staff->PlaceOfIssue}}"
                                               placeholder="Nhập vào nới cấp CMND. Ví dụ: Công an quận 1, TpHCM">
                                    </div>
                                </div>
                                <label for="email_address">Số Bảo hiểm xã hội</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="SocialInsuranceNo" class="form-control" value="{{$staff->SocialInsuranceNo}}"
                                               placeholder="Nhập vào nới cấp CMND. Ví dụ: 7908060474">
                                    </div>
                                </div>
                                <label for="email_address">Vai trò</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="RoleId">
                                            <option value="0">Không có vai trò</option>
                                            @if(count($role)!=0)
                                                @foreach($role as $item)
                                                    <option value="{{$item->RoleId}}" @if($item->RoleId==$staff->RoleId) selected @endif>{{$item->RoleName}}</option>
                                                @endforeach
                                            @endif
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <label for="email_address">Chức vụ</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="StaffTypeId">
                                            @if(count($stafftype)!=0)
                                                @foreach($stafftype as $item)
                                                    <option value="{{$item->StaffTypeId}}" @if($item->StaffTypeId==$staff->StaffTypeId) selected @endif>{{$item->StaffTypeName}}</option>
                                                @endforeach
                                            @endif
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <label for="email_address">Phòng ban</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="DepartmentId">
                                            @if(count($department)!=0)
                                                @foreach($department as $item)
                                                    <option value="{{$item->DepartmentId}}" @if($item->DepartmentId==$staff->DepartmentId) selected @endif>{{$item->DepartmentName}}</option>
                                                @endforeach
                                            @endif
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <label for="email_address">Nhóm ngạch</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="ScaleGroupId" class="form-control" value="{{$staff->ScaleGroupId}}"
                                               placeholder="Nhập vào Nhóm ngạch. Ví dụ: A1">
                                    </div>
                                </div>
                                <label for="email_address">Mã số ngạch</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="ScaleId" class="form-control" value="{{$staff->ScaleId}}"
                                               placeholder="Nhập vào Mã số ngạch. Ví dụ: 15.113">
                                    </div>
                                </div>
                                <label for="email_address">Bậc</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="number" name="Level" class="form-control" value="{{$staff->Level}}"
                                               placeholder="Nhập vào Bậc. Ví dụ: 1">
                                    </div>
                                </div>
                                <label for="email_address">Hệ số</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="number" name="Rate" class="form-control" value="{{$staff->Rate}}"
                                               placeholder="Nhập vào Hệ số. Ví dụ: 1">
                                    </div>
                                </div>
                                <label for="email_address">PC chức vụ</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="number" name="ResponsibilityPayment" class="form-control" value="{{$staff->ResponsibilityPayment}}"
                                               placeholder="Nhập vào PC chức vụ. Ví dụ: 1">
                                    </div>
                                </div>
                                <label for="email_address">PC thâm niên VK</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="number" name="SeniorityAllowance" class="form-control" value="{{$staff->SeniorityAllowance}}"
                                               placeholder="Nhập vào PC thâm niên VK. Ví dụ: 0">
                                    </div>
                                </div>
                                <label for="email_address">Văn hóa</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="EducationLevel" class="form-control" value="{{$staff->EducationLevel}}"
                                               placeholder="Nhập vào Văn hóa. Ví dụ: 12/12">
                                    </div>
                                </div>
                                <label for="email_address">Chuyên môn</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="Degree" class="form-control" value="{{$staff->Degree}}"
                                               placeholder="Nhập vào Chuyên môn. Ví dụ: Tiến sĩ">
                                    </div>
                                </div>
                                <label for="email_address">Chuyên ngành</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="MajorSubject" class="form-control" value="{{$staff->MajorSubject}}"
                                               placeholder="Nhập vào Chuyên ngành. Ví dụ: Quản lý Giáo dục">
                                    </div>
                                </div>
                                <label for="email_address">QLNN</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="ManagementLevel" class="form-control" value="{{$staff->ManagementLevel}}"
                                               placeholder="Nhập vào QLNN. Ví dụ: Trung cấp">
                                    </div>
                                </div>
                                <label for="email_address">Chính trị</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="PoliticLevel" class="form-control" value="{{$staff->PoliticLevel}}"
                                               placeholder="Nhập vào Chính trị. Ví dụ: Trung cấp">
                                    </div>
                                </div>
                                <label for="email_address">Trình độ ngoại ngữ</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="ForeignerLanguageLevel" class="form-control" value="{{$staff->ForeignerLanguageLevel}}"
                                               placeholder="Nhập vào trình độ ngoại ngữ. Ví dụ: Tiếng Anh Đại học">
                                    </div>
                                </div>
                                <label for="email_address">Tin học</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="ComputerSkillLevel" class="form-control" value="{{$staff->ComputerSkillLevel}}"
                                               placeholder="Nhập vào Tin học. Ví dụ: Trung cấp">
                                    </div>
                                </div>
                                <label for="email_address">Dân tộc</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="Nation" class="form-control" value="{{$staff->Nation}}"
                                               placeholder="Nhập vào dân tộc. Ví dụ: Kinh">
                                    </div>
                                </div>
                                <label for="email_address">Tôn giáo</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="Religion" class="form-control" value="{{$staff->Religion}}"
                                               placeholder="Nhập vào tôn giáo. Ví dụ: Không">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <label>Đảng viên</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                             <input type="radio" value="0" class="with-gap" @if(!$staff->IsAdherer) checked @endif name="IsAdherer" id="IsAdherer1">
                                            <label for="IsAdherer1" style="margin-right: 30px">Chưa là đảng viên</label>
                                            <input type="radio" class="with-gap" value="1" @if($staff->IsAdherer) checked @endif name="IsAdherer" id="IsAdherer">
                                            <label for="IsAdherer">Đã là đảng viên</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <label for="email_address">Ngày vào đảng</label>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" name="AdherentDate" class="form-control date" value="{{$staff->AdherentDate? \Carbon\Carbon::parse($staff->AdherentDate)->format('d/m/Y'):""}}"
                                               placeholder="Nhập vào ngày vào đảng. Ví dụ: 01/01/2001">
                                    </div>
                                </div>
                                <label for="email_address">Ngày chính thức vào đảng</label>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" name="OfficalAdherentDate" class="form-control date" value="{{$staff->OfficalAdherentDate? \Carbon\Carbon::parse($staff->OfficalAdherentDate)->format('d/m/Y'):""}}"
                                               placeholder="Nhập vào ngày chính thức vào đảng. Ví dụ: 01/01/2001">
                                    </div>
                                </div>
                                <label for="email_address">Nơi Sinh</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="PlaceOfBirth" class="form-control" value="{{$staff->PlaceOfBirth}}"
                                               placeholder="Nhập vào nơi sinh. Ví dụ: TP.HCM, Phường Tân Thới Hiệp, Quận 12, Thành phố Hồ Chí Minh">
                                    </div>
                                </div>
                                <label for="email_address">Quê quán</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="HomeTown" class="form-control" value="{{$staff->HomeTown}}"
                                               placeholder="Nhập vào Quê sinh. Ví dụ: TP.HCM, Phường Tân Thới Hiệp, Quận 12, Thành phố Hồ Chí Minh">
                                    </div>
                                </div>
                                <label for="email_address">Địa chỉ hiện tại</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" name="CurrentAddress" class="form-control" value="{{$staff->CurrentAddress}}"
                                               placeholder="Nhập vào Địa chỉ hiện tại. Ví dụ: TP.HCM, Phường Tân Thới Hiệp, Quận 12, Thành phố Hồ Chí Minh">
                                    </div>
                                </div>
                                <label for="email_address">Ngày Tham gia công tác</label>
                                <div class="input-group demo-masked-input">
                                    <div class="form-line">
                                        <input type="text" name="WorkingDate" class="form-control date" value="{{$staff->WorkingDate?\Carbon\Carbon::parse($staff->WorkingDate)->format('d/m/Y'):""}}"
                                               placeholder="Nhập vào ngày bắt đầu làm việc. Ví dụ: 01/01/2001">
                                    </div>
                                </div>

                                <label for="password">Review</label>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="radio" class="with-gap" value="1" @if($staff->IsReview) checked @endif name="IsReview" id="IsReview">
                                            <label for="IsReview" style="margin-right: 30px">Đã review</label>
                                             <input type="radio" value="0" class="with-gap" @if(!$staff->IsReview) checked @endif name="IsAdherer" id="IsReview1">
                                            <label for="IsReview1">Chưa review</label>
                                        </span>
                                    </div>
                                </div>
                                <label for="email_address">Ghi chú</label>
                                <div class="input-group">
                                    <div class="form-line">
                                        <textarea name="Note" class="form-control" placeholder="Nhập ghi chú...">{{$staff->Note}}</textarea>
                                    </div>
                                </div>
                                <button type="submit" value="thong-bao/nhan-vien" name="btnSave" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
