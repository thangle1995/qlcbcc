@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Nhân viên thay thế</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <form action="{{url('/thong-bao/su-kien/propose')}}/{{$event->EventId}}" method="post">
                            {{ csrf_field() }}
                            <div class="header">
                                @include('partial.alert')
                                <a href="{{url('/')}}/thong-bao/su-kien/{{$event->EventId}}" class="btn btn-lg btn-primary">Trở về</a>
                                <button type="submit" class="btn btn-lg btn-success">Cập nhật</button>
                            </div>
                            <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Vai trò</th>
                                        <th>Phòng</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($staff)
                                        @foreach($staff as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->StaffName}}</td>
                                                <td>{{$d->MifareCardId}}</td>
                                                <td>{{$d->StaffTypeName}}</td>
                                                <td>{{$d->RoleName}}</td>
                                                <td>{{$d->DepartmentName}}</td>
                                                <td>
                                                    <div class="input-group input-group-lg">
                                                        <span class="input-group-addon">
                                                            <input type="radio" value="{{$d->StaffId}}" class="with-gap" name="StaffId" id="{{$d->StaffId}}">
                                                            <label for="{{$d->StaffId}}"></label>
                                                        </span>
                                                    </div>
                                                    {{--<input type="radio" value="{{$d->StaffId}}" name="StaffId">--}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Vai trò</th>
                                        <th>Phòng</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
