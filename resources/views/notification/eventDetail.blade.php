@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Sự kiện tham gia</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> {{$event->Title}}</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/thong-bao/su-kien')}}" class="btn btn-lg btn-danger">Trở về</a>
                            @if($event->Status==1)
                                <a href="{{url('/thong-bao/su-kien/accept')}}/{{$event->EventId}}" class="btn btn-lg btn-success">Chấp nhận</a>
                                <a href="{{url('/thong-bao/su-kien/propose')}}/{{$event->EventId}}" class="btn btn-lg btn-success">Đề xuất người thay thế</a>
                            @endif
                            @if($event->Status==2)
                                <a disabled="disabled" class="btn btn-lg btn-success">Đã chấp nhận</a>
                                <a href="{{url('/thong-bao/su-kien/propose')}}/{{$event->EventId}}" class="btn btn-lg btn-success">Đề xuất người thay thế</a>
                            @endif
                            @if($event->Status==0)
                                <a disabled="disabled" class="btn btn-lg btn-success">Đã đề xuất người thay thế</a>
                            @endif

                        </div>
                        <div class="body">
                            @if($staffPropose)
                                <div class="clearfix">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                Thông tin người thay thế:
                                            </h2>
                                        </div>
                                        <div class="body">
                                            {{$staffPropose->StaffId}} - {{$staffPropose->StaffName}},
                                            <br> Phòng: {{$staffPropose->DepartmentName}},
                                            <br> Chức vụ: {{$staffPropose->StaffTypeName}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thông tin</th>
                                        <th>Nội dung</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Tên sự kiện</td>
                                                <td>{{$event->Title}}</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Thời gian bắt đầu</td>
                                                <td>{{\Carbon\Carbon::parse($event->StartDate)->format(' H:i d/m/Y')}}</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Thời gian kết thúc</td>
                                                <td>{{\Carbon\Carbon::parse($event->EndDate)->format(' H:i d/m/Y')}}</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Địa điểm</td>
                                                <td>{{$event->Address}}</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Nội dung</td>
                                                <td>{{$event->Description}}</td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2>
                                                    Danh sách tham gia ({{count($staff)}} người)
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Mã</th>
                                                            <th>Tên</th>
                                                            <th>Chức vụ</th>
                                                            <th>Phòng</th>
                                                            <th>Trạng thái</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if($staff)
                                                            @foreach($staff as $d)
                                                                <tr>
                                                                    <td>{{$loop->iteration}}</td>
                                                                    <td>{{$d->StaffId}}</td>
                                                                    <td>{{$d->StaffName}}</td>
                                                                    <td>{{$d->StaffTypeName}}</td>
                                                                    <td>{{$d->DepartmentName}}</td>
                                                                    <td>
                                                                        @if($d->Status==0)
                                                                            <label class="label label-danger">Đã đề xuất người khác</label>
                                                                        @endif
                                                                        @if($d->Status==1)
                                                                            <label class="label label-primary">Đang đợi xác nhận</label>
                                                                        @endif
                                                                        @if($d->Status==2)
                                                                                <label class="label label-success">Đã xác nhận</label>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if($d->ProposeByStaffId ==0)
                                                                            <label class="label label-primary">Được mời</label>
                                                                        @else
                                                                            <label class="label label-warning">Được đề xuất bởi <br>
                                                                                {!! \App\Http\Controllers\StaffController::getStaff($d->ProposeByStaffId)->StaffName !!}
                                                                            </label>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @else
                                                            <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                                        @endif

                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>STT</th>
                                                            <th>Mã</th>
                                                            <th>Tên</th>
                                                            <th>Chức vụ</th>
                                                            <th>Phòng</th>
                                                            <th>Trạng thái</th>
                                                            <th></th>
                                                        </tr>
                                                        </tfoot>

                                                    </table>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $('.propose').click(function (e) {
            var a_href = $(this).attr('href');
            e.preventDefault();
            $.ajax({
                url: a_href,
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $('body').prepend(data);
                    $('#myStaff').modal('show');
                }
            });
        });
    </script>
    @endsection
