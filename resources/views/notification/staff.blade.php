@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Nhân viên</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Vai trò</th>
                                        <th>Phòng</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($staff)
                                        @foreach($staff as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->StaffId}}</td>
                                                <td>{{$d->StaffName}}</td>
                                                <td>{{$d->MifareCardId}}</td>
                                                <td>{{$d->StaffTypeName}}</td>
                                                <td>{{$d->RoleName}}</td>
                                                <td>{{$d->DepartmentName}}</td>
                                                <td>
                                                    @if($d->IsReview)
                                                        <label class="label label-success">Đã duyệt</label>
                                                    @else
                                                        <label class="label label-danger">Chưa duyệt</label>
                                                    @endif
                                                </td>
                                                {{--<td>{!! \App\Http\Controllers\StaffController::getRole($d->RoleId) !!}</td>--}}
                                                {{--<td>{!! \App\Http\Controllers\StaffController::getDepartment($d->DepartmentId) !!}</td>--}}
                                                <td>
                                                    <a href="{{url('/')}}/thong-bao/nhan-vien/{{$d->StaffId}}" class="btn btn-sm btn-warning">Cập nhật</a>
                                                    <button onclick="deleteObj('.group-{{$d->StaffId}}','{{url('/')}}/nhan-vien/xoa/{{$d->StaffId}}','{{$d->StaffName}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Mã Thẻ</th>
                                        <th>Chức vụ</th>
                                        <th>Vai trò</th>
                                        <th>Phòng</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
