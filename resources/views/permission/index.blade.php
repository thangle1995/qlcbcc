@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Quyền truy cập</a></li>
                    <li class="active"><i class="material-icons">list</i> Danh sách</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/quyen/tao-moi" class="btn btn-lg btn-success">Thêm quyền mới</a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Tên</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($permission)
                                        @foreach($permission as $d)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$d->Name}}</td>
                                                <td>
                                                    <a href="{{url('/')}}/quyen/cap-nhat/{{$d->PermissionId}}" class="btn btn-sm btn-warning">Cập nhật</a>
                                                    <button onclick="deleteObj('.group-{{$d->PermissionId}}','{{url('/')}}/quyen/xoa/{{$d->PermissionId}}','{{$d->Name}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                </td>
                                            </tr>
                                            @if(count($d->child)!=0)
                                                @foreach($d->child as $item)
                                                    <tr>
                                                        <td></td>
                                                        <td> - {{$item->Name}}</td>
                                                        <td>
                                                            <a href="{{url('/')}}/quyen/cap-nhat/{{$item->PermissionId}}" class="btn btn-sm btn-warning">Cập nhật</a>
                                                            <button onclick="deleteObj('.group-{{$item->PermissionId}}','{{url('/')}}/quyen/xoa/{{$item->PermissionId}}','{{$item->Name}}')" class="btn btn-sm btn-danger">Xóa</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @else
                                        <h3 style="color:red;">Bạn không có quyền xem nội dung này.</h3>
                                    @endif


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Số thứ tự</th>
                                        <th>Tên</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
