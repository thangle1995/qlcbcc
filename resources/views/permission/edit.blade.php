@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Quyền truy cập</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Cập nhật</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/quyen/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/quyen/cap-nhat/{{$permission->PermissionId}}" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Tên quyền</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="Name" class="form-control"
                                               placeholder="Nhập vào tên quyền. Ví dụ: Quản lý Sự kiện" value="{{$permission->Name}}">
                                    </div>
                                </div>
                                <label for="email_address">Đường dẫn</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="Link" class="form-control"
                                               placeholder="Nhập vào đường dẫn. Ví dụ: #" value="{{$permission->Link}}">
                                    </div>
                                </div>
                                <label for="email_address">Danh mục quyền</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="ParentId">
                                            <option value="0">Danh mục gốc</option>
                                            @foreach($category as $item)
                                                <option @if($item->PermissionId==$permission->ParentId) selected @endif value="{{$item->PermissionId}}">{{$item->Name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Cập nhật</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
