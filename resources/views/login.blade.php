<!DOCTYPE html>
<html style="height: 100%;">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Phần mềm QL Quận 12</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    {{--<link href="../../plugins/node-waves/waves.css" rel="stylesheet" />--}}

    <!-- Animation Css -->
    <link href="{{url('/')}}/css/animate.min.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{url('/')}}/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <p class="text-center"><img src="{{asset('/')}}img/logo.png" alt="Logo Quan 12"/></p>
        <b>PHẦN MỀM QUẢN LÝ CBCC Q.12</b>
        {{--<small>Admin BootStrap Based - Material Design</small>--}}
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_in" action="{{url('/login')}}" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <div class="msg">Đăng nhập</div>
                @include('partial.alert')
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="staffId" placeholder="Mã nhân viên" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Mật khẩu" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button class="btn btn-block bg-pink waves-effect" type="submit">ĐĂNG NHẬP</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{url('/')}}/js/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="{{url('/')}}/js/bootstrap.min.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{url('/')}}/js/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="{{url('/')}}/js/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="{{url('/')}}/js/admin.js"></script>
<script src="{{url('/')}}/js/sign-in.js"></script>
</body>

</html>