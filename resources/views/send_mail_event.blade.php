<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>PHẦN MỀN QUẢN LÝ CÁN BỘ CÔNG CHỨC QUẬN 12</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{url('/css/bootstrap.css')}}" rel="stylesheet">
</head>
<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 10px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"
                   style="border: 1px solid #cccccc; border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#70bbd9"
                        style="padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                        <img src="http://quanlynhansu.website/img/logo.png" alt="quận 12" width="200" height="200"
                             style="display: block;"/>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                    <b>{{$event_name}}</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Kính gửi anh/chị {{$staff_name}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Quận 12 xin thông báo sự kiện của anh/chị như sau:
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Tên sự kiện: {{$event_name}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Thời gian: Từ {{$start}} - Đến {{$end}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Địa điểm: {{$address}}
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    Nội dung: {{$description}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center"
                                    style="padding-top:20px; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    <a href="{{url('confirm')}}/{{$event_staff_id}}/{{$token}}" class="btn btn-success">Xác
                                        nhận</a>
                                    <a href="{{url('/')}}" target="_blank" class="btn btn-danger"
                                       style="margin-left: 10px">Đề xuất</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;"
                                    width="75%">
                                    Ủy ban nhân dân quận 12
                                    {{--<a href="#" style="color: #ffffff;"><font color="#ffffff">Unsubscribe</font></a> to this newsletter instantly--}}
                                </td>
                                <td align="right" width="25%">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                            <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                                                <a href="https://www.facebook.com/ubnd.q12/" style="color: #ffffff;">
                                                    <img src="http://quanlynhansu.website/img/fb.gif" alt="Facebook"
                                                         width="38" height="38" style="display: block;" border="0"/>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>