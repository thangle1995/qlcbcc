@extends('layout.master')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>DANH SÁCH PHÒNG BAN</h2>--}}
                <ol class="breadcrumb breadcrumb-bg-blue">
                    <li><a href="javascript:void(0);"><i class="material-icons">home</i> Trang chủ</a></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">business</i> Chức vụ</a></li>
                    <li class="active"><i class="material-icons">check_circle</i> Thêm mới</li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            @include('partial.alert')
                            <a href="{{url('/')}}/chuc-vu/danh-sach" class="btn btn-lg btn-danger">Trở về</a>
                        </div>
                        <div class="body">
                            <form action="{{url('/')}}/chuc-vu/tao-moi" method="post">
                                {{ csrf_field() }}
                                <label for="email_address">Tên Chức vụ</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="StaffTypeName" class="form-control" value="{{old("StaffTypeName")}}"
                                               placeholder="Nhập vào tên chức vụ. Ví dụ: Nhân viên">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-addon" style="text-align: left">
                                            <input type="checkbox" class="with-gap" name="Type" id="ig_radio">
                                            <label for="ig_radio" style="margin-right: 30px">Được phép xem đơn nghỉ phép</label>
                                        </span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success m-t-15 waves-effect">Thêm chức vụ</button>
                                <button type="reset" class="btn btn-danger m-t-15 waves-effect">Hủy</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
