## Development

Follow step by step in below:

`git clone https://gitlab.com/thangle1995/qlcbcc.git`

`composer install`

`cp .env.example .env`

`php artisan migrate`

`php artisan db:seed`

Make sure you installed OCDB connect to sql server already.
