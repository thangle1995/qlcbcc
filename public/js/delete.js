function deleteObj(obj,url,text) {
    $.confirm({
        icon: 'fa fa-warning',
        title: 'Xác nhận xóa',
        content: 'Bạn có chắc muốn xóa <strong>'+text+'</strong> ?',
        type: 'red',
        theme: 'light',
        typeAnimated: true,
        buttons: {
            ok: {
                text: 'Xóa',
                btnClass:'btn-red',
                action:function () {
                    window.location.href=url;
                    // $.ajax({
                    //     url: url,
                    //     type: 'DELETE',
                    //     dataType:'json',
                    //     success: function(res) {
                    //         if(res.status){
                    //             Notify('Thông báo',res.stt,'success');
                    //             $(obj).parent().parent().fadeOut('fast');
                    //             if(typeof callback_after_delete !== 'undefined')
                    //                 callback_after_delete(res);
                    //         }
                    //         else
                    //             Notify('Lỗi',res.stt,'error');
                    //     },
                    //     error:function(e){
                    //         Notify('Lỗi',e.statusText,'error');
                    //     }
                    // });
                }
            },
            close: {
                text: 'Đóng'
            }
        }
    });
}